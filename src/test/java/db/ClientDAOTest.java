/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author marko
 */
public class ClientDAOTest {
    private static ClientDAO clientDAO;
    
    public ClientDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
//        otvaramo konekciju ka bazi
        DatabaseConnection.getConnection();        
        ClientDAOTest.clientDAO = new ClientDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
//        zatvaramo konekciju ka bazi
        DatabaseConnection.close();        
        ClientDAOTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientDAOTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of convertFromResultSet method, of class ClientDAO.
     */
    /**
     * Testiramo convertFromResultSet za ClientDAO-a
     * 
     * ukoliko rezultat za sva polja koja su obavezna za entitet sadrzi neku vrednost,
     * znaci nisu prazna ocekujemo da konvertovanje vrati objekat koji nije null, 
     * u suprotnom vazi obrnuto
     */
    @Test
    public void testConvertFromResultSet() {
        System.out.println("testConvertFromResultSet - Result je null, vracena vrednost bi trebala biti null");
        
        ResultSet rs = null;
        
        Client expResult = null;
        Client result = ClientDAOTest.clientDAO.convertFromResultSet(rs);
        assertEquals(expResult, result);
    }
    
//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of updateOne method, of class ClientDAO.
     */
    /**
     * Testiramo UPDATE za ClientDAO-a (Tabela odlucivanja i uzrocno-posledicni graf)
     * 
     * ukoliko Client kojeg zelimo da UPDATE-ujemo nema za neko obavezno polje
     * vrednost ocekujemo rezultat false, u suprotnom true
     */    
    @Test
    public void updateTOUPGClient() {
        System.out.println("updateTOUPGClient - Client nije null i sva obavezna polja nisu prazna");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setName("Marko");
        onlyOne.setUsername("marko");
        onlyOne.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNull() {
        System.out.println("updateTOUPGClientNull - Client je null");
        
        Client object = null;
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(object);
        assertEquals(expResult, result);
    }
    
    
    @Test
    public void updateTOUPGClientNoNUP() {
        System.out.println("updateTOUPGClientNoNUP - Client nije null, ali nema ime, korisnicko ime i lozinku");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setName("");
        onlyOne.setUsername("");
        onlyOne.setPassword("");
        
//        ocekujem false buduci da klijent nema popunjene sve obavezne podatke
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNoN() {
        System.out.println("updateTOUPGClientNoN - Client nije null, ali nema ime");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setName("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNoNP() {
        System.out.println("updateTOUPGClientNoNP - Client nije null, ali nema ime, ni lozinku");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setName("");
        onlyOne.setPassword("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }    
    
    @Test
    public void updateTOUPGClientNoNU() {
        System.out.println("updateTOUPGClientNoNU - Client nije null, ali nema ime, ni korisnicko ime");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setName("");
        onlyOne.setUsername("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNoP() {
        System.out.println("updateTOUPGClientNoP - Client nije null, ali nema lozinku");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setPassword("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNoPU() {
        System.out.println("updateTOUPGClientNoPU - Client nije null, ali nema lozinku, ni korisnicko ime");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setUsername("");
        onlyOne.setPassword("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);        
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGClientNoU() {
        System.out.println("updateTOUPGClientNoU - Client nije null, ali nema  korisnicko ime");
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        onlyOne.setUsername("");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of insertOne method, of class ClientDAO.
     */

    /**
     * Testiramo INSERT za ClientDAO-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * ukoliko klijent nije null i svi obavezni atributi nisu prazni result treba
     * da bude true, u suprotnom false
     */
    @Test
    public void insertTOUPGClient() {
        System.out.println("insertTOUPGClient - Client nije null i sva obavezna polja nisu prazna");
        
        Client object = new Client();
        object.setName("David");
        object.setUsername("david");
        object.setPassword("david");
        
        boolean expResult = true;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNull() {
        System.out.println("insertTOUPGClientNull - Client je null objekat");
        
        Client object = null;
        
//        ocekujemo false buduci da nam je klijent null object
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoNUP() {
        System.out.println("insertTOUPGClientNoNUP - Client nije null, ali nema ime, korisnicko ime i lozinku");
        
        Client object = new Client();
        
//        ocekujem false buduci da klijent nema popunjene sve obavezne podatke
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoN() {
        System.out.println("insertTOUPGClientNoN - Client nije null, ali nema ime");
        
        Client object = new Client();
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoNP() {
        System.out.println("insertTOUPGClientNoNP - Client nije null, ali nema ime, ni lozinku");
        
        Client object = new Client();
        object.setUsername("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }    
    
    @Test
    public void insertTOUPGClientNoNU() {
        System.out.println("insertTOUPGClientNoNU - Client nije null, ali nema ime, ni korisnicko ime");
        
        Client object = new Client();
        object.setPassword("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoP() {
        System.out.println("insertTOUPGClientNoP - Client nije null, ali nema lozinku");
        
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoPU() {
        System.out.println("insertTOUPGClientNoPU - Client nije null, ali nema lozinku, ni korisnicko ime");
        
        Client object = new Client();
        object.setName("Marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGClientNoU() {
        System.out.println("insertTOUPGClientNoU - Client nije null, ali nema  korisnicko ime");
        
        Client object = new Client();
        object.setName("Marko");
        object.setPassword("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGJedinstvenostKorisnickogImena() {
        System.out.println("insertTOUPGJedinstvenostKorisnickogImena - Provera jedinstvenosti korisnickog imena");
        
//        posto se pre poziva svakog testa kreira Client sa korisnickim imenom marko i ovde kreiramo
//        Client-a sa istim korisnickim imenom

        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = false;
        boolean result = ClientDAOTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
        
        ArrayList<Client> clients = ClientDAOTest.clientDAO.getAll();
        int numOfClients = 0;
        for(Client client : clients){
            if(client.getUsername().equals(object.getUsername())){
                numOfClients ++;
            }
        }
        assertEquals(1, numOfClients);
    }
}
