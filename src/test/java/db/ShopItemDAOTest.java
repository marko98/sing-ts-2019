/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemDAOTest {
    private static ShopItemDAO shopItemDAO;
    
    public ShopItemDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemDAOTest.shopItemDAO = new ShopItemDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        ShopItemDAOTest.shopItemDAO = null;
    }
    
    @Before
    public void setUp() {
        ShopItem object = new ShopItem();
        object.setName("Muski sampon za kosu");
        object.setAmount(200);
        object.setPrice(100);
        
        boolean expResult = true;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemDAOTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        }); 
    }
    
//    -----------------------------------------------------------------------------------------------------
    /**
     * obavezna polja: ime, cena, kolicina
     * 
     * cena, kolicina ne smeju biti negativne vrednosti!
     */

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of convertFromResultSet method, of class ShopItemDAO.
     */
    /**
     * Testiramo convertFromResultSet za ShopItemDAO
     * 
     * ukoliko rezultat za sva polja koja su obavezna za entitet sadrzi neku vrednost,
     * znaci nisu prazna ocekujemo da konvertovanje vrati objekat koji nije null, 
     * u suprotnom vazi obrnuto
     */
    @Test
    public void testConvertFromResultSet() {
        System.out.println("testConvertFromResultSet - Result je null, vracena vrednost bi trebala biti null");
        
        ResultSet rs = null;
        
        ShopItem expResult = null;
        ShopItem result = ShopItemDAOTest.shopItemDAO.convertFromResultSet(rs);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of updateOne method, of class ShopItemDAO.
     */
    /**
     * Testiramo UPDATE za ShopItemDAO (Tabela odlucivanja i uzrocno-posledicni graf)
     * 
     * ukoliko ShopItem kojeg zelimo da UPDATE-ujemo nema za neko obavezno polje
     * vrednost ocekujemo rezultat false, u suprotnom true
     */   
    @Test
    public void updateTOUPGShopItem() {
        System.out.println("updateTOUPGShopItem - ShopItem nije null i sva obavezna polja nisu prazna");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        onlyOne.setName("olovka");
        onlyOne.setAmount(100);
        onlyOne.setPrice(50);
        
        boolean expResult = true;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNull() {
        System.out.println("updateTOUPGShopItemNull - ShopItem je null");
        
        ShopItem object = null;
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaImeKolicinuCenu() {
        System.out.println("updateTOUPGShopItemNemaImeKolicinuCenu - ShopItem nije null, ali nema ime, kolicinu i cenu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaIme() {
        System.out.println("updateTOUPGShopItemNemaIme - ShopItem nije null, ali nema ime");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        onlyOne.setName("");
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaImeCenu() {
        System.out.println("updateTOUPGShopItemNemaImeCenu - ShopItem nije null, ali nema ime, ni cenu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        newOne.setAmount(100);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }    
    
    @Test
    public void updateTOUPGShopItemNemaImeKolicinu() {
        System.out.println("updateTOUPGShopItemNemaImeKolicinu - ShopItem nije null, ali nema ime, ni kolicinu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        newOne.setPrice(50);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaCenu() {
        System.out.println("updateTOUPGShopItemNemaCenu - ShopItem nije null, ali nema cenu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("Voda");
        newOne.setAmount(100);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaCenuKolicinu() {
        System.out.println("updateTOUPGShopItemNemaCenuKolicinu - ShopItem nije null, ali nema cenu, ni kolicinu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("Voda");
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);        
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGShopItemNemaKolicinu() {
        System.out.println("updateTOUPGShopItemNemaKolicinu - ShopItem nije null, ali nema kolicinu");
        
        ArrayList<ShopItem> shopItems = ShopItemDAOTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        ShopItem newOne = new ShopItem();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("Voda");
        newOne.setPrice(50);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of insertOne method, of class ShopItemDAO.
     */
    /**
     * Granicne vrednosti i klase ekvivalencije
     * 
     * legalne:
     *  cena i kolicina >= 0
     * ilegalne:
     *  cena i kolicina <0
     */
    @Test
    public void insertGVIKECenaNegativna() {
        System.out.println("insertGVIKECenaNegativna - sva ostala polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        object.setPrice(-30);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKECenaNula() {
        System.out.println("insertGVIKECenaNula - sva polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        object.setPrice(0);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKECenaPozitivna() {
        System.out.println("insertGVIKECenaPozitivna - sva polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        object.setPrice(50);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEKolicinaNegativna() {
        System.out.println("insertGVIKEKolicinaNegativna - sva ostala polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(-100);
        object.setPrice(50);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEKolicinaNula() {
        System.out.println("insertGVIKEKolicinaNula - sva polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(0);
        object.setPrice(50);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEKolicinaPozitivna() {
        System.out.println("insertGVIKEKolicinaPozitivna - sva polja zadovoljavaju kriterijume");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        object.setPrice(50);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    /**
     * Testiramo INSERT za ShopItemDAO-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * obavezna polja: ime, cena, kolicina
     * 
     * ukoliko ni jedno polje nije prazno ocekujemo true, u suprotnom false
     */
 
    @Test
    public void insertTOUPGShopItem() {
        System.out.println("insertTOUPGShopItem - ShopItem nije null i sva obavezna polja nisu prazna");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        object.setPrice(50);
        
        boolean expResult = true;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNull() {
        System.out.println("insertTOUPGShopItemNull - ShopItem je null");
        
        ShopItem object = null;
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaImeKolicinuCenu() {
        System.out.println("insertTOUPGShopItemNemaImeKolicinuCenu - ShopItem nije null, ali nema ime, kolicinu i cenu");
        
        ShopItem object = new ShopItem();
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaIme() {
        System.out.println("insertTOUPGShopItemNemaIme - ShopItem nije null, ali nema ime");
        
        ShopItem object = new ShopItem();
        object.setAmount(100);
        object.setPrice(50);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaImeCenu() {
        System.out.println("insertTOUPGShopItemNemaImeCenu - ShopItem nije null, ali nema ime, ni cenu");
        
        ShopItem object = new ShopItem();
        object.setAmount(100);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }    
    
    @Test
    public void insertTOUPGShopItemNemaImeKolicinu() {
        System.out.println("insertTOUPGShopItemNemaImeKolicinu - ShopItem nije null, ali nema ime, ni kolicinu");
        
        ShopItem object = new ShopItem();
        object.setPrice(50);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaCenu() {
        System.out.println("insertTOUPGShopItemNemaCenu - ShopItem nije null, ali nema cenu");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setAmount(100);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaCenuKolicinu() {
        System.out.println("insertTOUPGShopItemNemaCenuKolicinu - ShopItem nije null, ali nema cenu, ni kolicinu");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGShopItemNemaKolicinu() {
        System.out.println("insertTOUPGShopItemNemaKolicinu - ShopItem nije null, ali nema kolicinu");
        
        ShopItem object = new ShopItem();
        object.setName("olovka");
        object.setPrice(50);
        
        boolean expResult = false;
        boolean result = ShopItemDAOTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
}