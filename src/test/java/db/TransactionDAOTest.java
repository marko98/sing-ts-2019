/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class TransactionDAOTest {
    private static TransactionDAO transactionDAO;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ShopItemDAO shopItemDAO;
    private static ClientDAO clientDAO;
    
    public TransactionDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        TransactionDAOTest.transactionDAO = new TransactionDAO();
        
        TransactionDAOTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionDAOTest.shopItemDAO = new ShopItemDAO();
        TransactionDAOTest.clientDAO = new ClientDAO();
        
//        napravimo jednog klijenta
        Client client = new Client();
        client.setName("Marko");
        client.setUsername("marko");
        client.setPassword("marko");
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.clientDAO.insertOne(client);
        assertEquals(expResult, result);
        
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(100);
        
        expResult = true;
        result = TransactionDAOTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
       
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = TransactionDAOTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = TransactionDAOTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionDAOTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = TransactionDAOTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        TransactionDAOTest.transactionDAO = null;
        
        TransactionDAOTest.deliveryServiceDAO = null;
        TransactionDAOTest.shopItemDAO = null;
        TransactionDAOTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
//        napravimo jednu transakcije
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(20); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Transaction> transactions = TransactionDAOTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = TransactionDAOTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
    }
    /**
     * odabrana kolicina mora biti manja ili jednaka dostupnoj kolicini proizvoda
     * 
     * udaljenost ne moze biti negativna
     */

//    -----------------------------------------------------------------------------------------------------  
    /**
     * Test of convertFromResultSet method, of class TransactionDAO.
     */
    /**
     * Testiramo convertFromResultSet za TransactionDAO
     * 
     * ukoliko rezultat za sva polja koja su obavezna za entitet sadrzi neku vrednost,
     * znaci nisu prazna ocekujemo da konvertovanje vrati objekat koji nije null, 
     * u suprotnom vazi obrnuto
     */
    @Test
    public void testConvertFromResultSet() {
        System.out.println("testConvertFromResultSet - Result je null, vracena vrednost bi trebala biti null");
        
        ResultSet rs = null;
        
        Transaction expResult = null;
        Transaction result = TransactionDAOTest.transactionDAO.convertFromResultSet(rs);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------  
    /**
     * Test of insertOne method, of class TransactionDAO.
     */
    /**
     * Granicne vrednosti i klase ekvivalencije
     * 
     * odabrana kolicina <= dostupna kolicina proizvoda
     * udaljenost >= 0
     * 
     * legalne:
     *  odabrana kolicina <= dostupna kolicina proizvoda i udaljenost >= 0
     * ilegalne:
     *  odabrana kolicina > dostupna kolicina proizvoda i udaljenost < 0
     */
    
    @Test
    public void insertGVIKEUdaljenostNegativna() {
        System.out.println("insertGVIKEUdaljenostNegativna - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(20); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(-1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = false;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEUdaljenostNula() {
        System.out.println("insertGVIKEUdaljenostNula - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(20); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(0);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEUdaljenostPozitivna() {
        System.out.println("insertGVIKEUdaljenostPozitivna - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(20); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEOdabranaKolicinaManjaOdDostupneKolicine() {
        System.out.println("insertGVIKEOdabranaKolicinaManjaOdDostupneKolicine - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(20); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEOdabranaKolicinaJednakaDostupnojKolicini() {
        System.out.println("insertGVIKEOdabranaKolicinaJednakaDostupnojKolicini - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(200); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEOdabranaKolicinaVecaOdDostupneKolicine() {
        System.out.println("insertGVIKEOdabranaKolicinaVecaOdDostupneKolicine - sva ostala polja zadovoljavaju kriterijume");
        
        ArrayList<Client> clients = TransactionDAOTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionDAOTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionDAOTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(250); // ukupno ima 200, preostalo 180
        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = false;
        boolean result = TransactionDAOTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
}
