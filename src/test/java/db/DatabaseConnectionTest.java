/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.*;

/**
 *
 * @author marko
 */
public class DatabaseConnectionTest {
    
    public DatabaseConnectionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getConnection method, of class DatabaseConnection.
     */
    @Test
    public void testGetConnection() {
        System.out.println("testGetConnection");
        Connection result = DatabaseConnection.getConnection();
        assertNotEquals(null, result);
    }

    /**
     * Test of close method, of class DatabaseConnection.
     */
    /**
     * Kako proveriti da li je konekcija zatvorena kada je polje connection staticko 
     * i privatno, a getter za connection, ukoliko je connection null,
     * vrsi konektovanje
     */
    @Test
    public void testClose() {
        System.out.println("close");
        DatabaseConnection.close();
    }
    
}
