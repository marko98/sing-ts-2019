/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.Client;
import model.Entity;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class GenericDAOTest {
    private static ClientDAO clientDAO;
    
    public GenericDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        GenericDAOTest.clientDAO = new ClientDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        GenericDAOTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
        Client object = new Client();
        object.setName("David");
        object.setUsername("david");
        object.setPassword("david");
        
        boolean expResult = true;
        boolean result = GenericDAOTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Client> clients = GenericDAOTest.clientDAO.getAll();
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = GenericDAOTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
    }

    /**
     * Test of getAll method, of class GenericDAO.
     */
    @Test
    public void testGetAll() {
        System.out.println("testGetAll");
        
        int expResult = 1;
        ArrayList<Client> result = GenericDAOTest.clientDAO.getAll();
        assertEquals(expResult, result.size());
    }

    /**
     * Test of getOne method, of class GenericDAO.
     * 
     * metode crne kutije - analiza granicnih vrednosti
     * 
     * za negativan id ocekujemo null, za ostale ukoliko
     * Client sa prosledjenim id-jem postoji ne ocekujemo null
     */
    @Test
    public void aGVGetOneNV() {
        System.out.println("aGVGetOneNV - negativna vrednost");
        
        Entity expResult = null;
        Client result = GenericDAOTest.clientDAO.getOne(-1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void aGVGetOnePVNP() {
        System.out.println("aGVGetOnePVNP - pozitivna vrednost, Client sa prosledjenim id-jem ne postoji");
        
        ArrayList<Client> clients = GenericDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        int id = onlyOne.getId();
        GenericDAOTest.clientDAO.deleteOne(id);
        
        Entity expResult = null;
        Client result = GenericDAOTest.clientDAO.getOne(id);
        assertEquals(expResult, result);
    }
    
    @Test
    public void aGVGetOnePVP() {
        System.out.println("aGVGetOnePVP - pozitivna vrednost, Client sa prosledjenim id-jem postoji");
        
        ArrayList<Client> clients = GenericDAOTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        Client result = GenericDAOTest.clientDAO.getOne(onlyOne.getId());
        assertNotEquals(null, result);
    }

    /**
     * Test of deleteOne method, of class GenericDAO.
     *
     * za bilo koji id ocekujemo true
     */
    @Test
    public void testDeleteOneNegativeOne() {
        System.out.println("testDeleteOneNegativeOne");
        
        boolean expResult = true;
        boolean result = GenericDAOTest.clientDAO.deleteOne(-1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDeleteOneZero() {
        System.out.println("testDeleteOneZero");
        
        boolean expResult = true;
        boolean result = GenericDAOTest.clientDAO.deleteOne(0);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testDeleteOnePositiveOne() {
        System.out.println("testDeleteOnePositiveOne");
        
        boolean expResult = true;
        boolean result = GenericDAOTest.clientDAO.deleteOne(1);
        assertEquals(expResult, result);
    }

    public class GenericDAOImpl extends GenericDAO {
    }
    
}
