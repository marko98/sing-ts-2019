/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class DeliveryServiceDAOTest {
    private static DeliveryServiceDAO deliveryServiceDAO;
    
    public DeliveryServiceDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        DeliveryServiceDAOTest.deliveryServiceDAO = new DeliveryServiceDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        DeliveryServiceDAOTest.deliveryServiceDAO = null;
    }
    
    @Before
    public void setUp() {
        DeliveryService object = new DeliveryService();
        object.setName("Amazaon"); // UPS
        object.setStartingPrice(1000);
        object.setPricePerKilometer(200);
        
        boolean expResult = true;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        }); 
    }
    
    /**
     * obavezna polja: ime, pocetnaCena, cenaPoKilometru
     * 
     * pocetnaCena, cenaPoKilometru moraju biti vece od 0!
     */
    
//    -----------------------------------------------------------------------------------------------------    
    /**
     * Test of convertFromResultSet method, of class DeliveryServiceDAO.
     */
    /**
     * Testiramo convertFromResultSet za DeliveryServiceDAO
     * 
     * ukoliko rezultat za sva polja koja su obavezna za entitet sadrzi neku vrednost,
     * znaci nisu prazna ocekujemo da konvertovanje vrati objekat koji nije null, 
     * u suprotnom vazi obrnuto
     */
    @Test
    public void testConvertFromResultSet() {
        System.out.println("testConvertFromResultSet - Result je null, vracena vrednost bi trebala biti null");
        
        ResultSet rs = null;
        
        DeliveryService expResult = null;
        DeliveryService result = DeliveryServiceDAOTest.deliveryServiceDAO.convertFromResultSet(rs);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of updateOne method, of class DeliveryServiceDAO.
     */
    /**
     * Testiramo UPDATE za DeliveryServiceDAO (Tabela odlucivanja i uzrocno-posledicni graf)
     * 
     * ukoliko DeliveryService kojeg zelimo da UPDATE-ujemo nema za neko obavezno polje
     * vrednost ocekujemo rezultat false, u suprotnom true
     */   
    @Test
    public void updateTOUPGDeliveryService() {
        System.out.println("updateTOUPGDeliveryService - DeliveryService nije null i sva obavezna polja nisu prazna");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        onlyOne.setName("UPS");
        onlyOne.setStartingPrice(1000);
        onlyOne.setPricePerKilometer(200);
        
        boolean expResult = true;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNull() {
        System.out.println("updateTOUPGDeliveryServiceNull - DeliveryService je null");
        
        DeliveryService object = null;
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaImePocetnuCenuCenuPoKilometru() {
        System.out.println("updateTOUPGDeliveryServiceNemaImePocetnuCenuCenuPoKilometru - DeliveryService nije null, ali nema ime, pocetnuCenu i cenuPoKilometru");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaIme() {
        System.out.println("updateTOUPGDeliveryServiceNemaIme - DeliveryService nije null, ali nema ime");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        onlyOne.setName("");
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaImeCenuPoKilometru() {
        System.out.println("updateTOUPGDeliveryServiceNemaImeCenuPoKilometru - DeliveryService nije null, ali nema ime, ni cenuPoKilometru");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        newOne.setStartingPrice(1000);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }    
    
    @Test
    public void updateTOUPGDeliveryServiceNemaImePocetnuCenu() {
        System.out.println("updateTOUPGDeliveryServiceNemaImePocetnuCenu - DeliveryService nije null, ali nema ime, ni pocetnuCenu");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        newOne.setPricePerKilometer(200);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaCenuPoKilometru() {
        System.out.println("updateTOUPGDeliveryServiceNemaCenuPoKilometru - DeliveryService nije null, ali nema cenuPoKilometru");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("UPS");
        newOne.setStartingPrice(1000);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaCenuPoKilometruPocetnuCenu() {
        System.out.println("updateTOUPGDeliveryServiceNemaCenuPoKilometruPocetnuCenu - DeliveryService nije null, ali nema cenuPoKilometru, ni pocetnuCenu");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("UPS");
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);        
        assertEquals(expResult, result);
    }
    
    @Test
    public void updateTOUPGDeliveryServiceNemaPocetnuCenu() {
        System.out.println("updateTOUPGDeliveryServiceNemaPocetnuCenu - DeliveryService nije null, ali nema pocetnuCenu");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceDAOTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        DeliveryService newOne = new DeliveryService();
        newOne.setId(onlyOne.getId());
        
        newOne.setName("UPS");
        newOne.setPricePerKilometer(200);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.updateOne(newOne);
        assertEquals(expResult, result);
    }

//    -----------------------------------------------------------------------------------------------------
    /**
     * Test of insertOne method, of class DeliveryServiceDAO.
     */
    /**
     * Granicne vrednosti i klase ekvivalencije
     * pocetnaCena, cenaPoKilometru moraju biti vece od 0!
     * 
     * legalne:
     *  pocetnaCena i cenaPoKilometru > 0
     * ilegalne:
     *  pocetnaCena i cenaPoKilometru =<0
     */
    @Test
    public void insertGVIKEPocetnaCenaNegativna() {
        System.out.println("insertGVIKEPocetnaCenaNegativna - sva ostala polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(-1000);
        object.setPricePerKilometer(200);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEPocetnaCenaNula() {
        System.out.println("insertGVIKEPocetnaCenaNula - sva polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(0);
        object.setPricePerKilometer(200);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKEPocetnaCenaPozitivna() {
        System.out.println("insertGVIKEPocetnaCenaPozitivna - sva polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        object.setPricePerKilometer(200);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKECenaPoKilometruNegativna() {
        System.out.println("insertGVIKECenaPoKilometruNegativna - sva ostala polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        object.setPricePerKilometer(-200);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKECenaPoKilometruNula() {
        System.out.println("insertGVIKECenaPoKilometruNula - sva polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        object.setPricePerKilometer(0);
        
//        ocekujemo false
        boolean expResult = false;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertGVIKECenaPoKilometruPozitivna() {
        System.out.println("insertGVIKECenaPoKilometruPozitivna - sva polja zadovoljavaju kriterijume");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        object.setPricePerKilometer(200);
        
//        ocekujemo true
        boolean expResult = true;
        
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    /**
     * Testiramo INSERT za DeliveryServiceDAO-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * obavezna polja: ime, pocetnaCena, cenaPoKilometru
     * 
     * ukoliko ni jedno polje nije prazno ocekujemo true, u suprotnom false
     */
 
    @Test
    public void insertTOUPGDeliveryService() {
        System.out.println("insertTOUPGDeliveryService - DeliveryService nije null i sva obavezna polja nisu prazna");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        object.setPricePerKilometer(200);
        
        boolean expResult = true;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNull() {
        System.out.println("insertTOUPGDeliveryServiceNull - DeliveryService je null");
        
        DeliveryService object = null;
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaImePocetnuCenuCenuPoKilometru() {
        System.out.println("insertTOUPGDeliveryServiceNemaImePocetnuCenuCenuPoKilometru - DeliveryService nije null, ali nema ime, kolicinu i cenu");
        
        DeliveryService object = new DeliveryService();
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaIme() {
        System.out.println("insertTOUPGDeliveryServiceNemaIme - DeliveryService nije null, ali nema ime");
        
        DeliveryService object = new DeliveryService();
        object.setStartingPrice(1000);
        object.setPricePerKilometer(200);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaImeCenuPoKilometru() {
        System.out.println("insertTOUPGDeliveryServiceNemaImeCenuPoKilometru - DeliveryService nije null, ali nema ime, ni cenuPoKilometru");
        
        DeliveryService object = new DeliveryService();
        object.setStartingPrice(1000);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }    
    
    @Test
    public void insertTOUPGDeliveryServiceNemaImeKolicinu() {
        System.out.println("insertTOUPGDeliveryServiceNemaImeKolicinu - DeliveryService nije null, ali nema ime, ni pocetnuCenu");
        
        DeliveryService object = new DeliveryService();
        object.setPricePerKilometer(200);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaCenuPoKilometru() {
        System.out.println("insertTOUPGDeliveryServiceNemaCenuPoKilometru - DeliveryService nije null, ali nema cenuPoKilometru");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setStartingPrice(1000);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaCenuPoKilometruPocetnuCenu() {
        System.out.println("insertTOUPGDeliveryServiceNemaCenuPoKilometruPocetnuCenu - DeliveryService nije null, ali nema cenuPoKilometru, ni pocetnuCenu");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @Test
    public void insertTOUPGDeliveryServiceNemaPocetnuCenu() {
        System.out.println("insertTOUPGDeliveryServiceNemaPocetnuCenu - DeliveryService nije null, ali nema pocetnuCenu");
        
        DeliveryService object = new DeliveryService();
        object.setName("UPS");
        object.setPricePerKilometer(200);
        
        boolean expResult = false;
        boolean result = DeliveryServiceDAOTest.deliveryServiceDAO.insertOne(object);
        assertEquals(expResult, result);
    }
}
