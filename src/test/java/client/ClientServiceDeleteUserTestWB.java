/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ClientService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ClientServiceDeleteUserTestWB {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    public ClientServiceDeleteUserTestWB() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceDeleteUserTestWB.clientDAO = new ClientDAO();
        ClientServiceDeleteUserTestWB.instance = new ClientService();
    }
    
    @AfterClass
    public static void tearDownClass() {        
        DatabaseConnection.close();
        ClientServiceDeleteUserTestWB.clientDAO = null;
        ClientServiceDeleteUserTestWB.instance = null;
    }
    
    @Before
    public void setUp() {
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientServiceDeleteUserTestWB.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Client> clients = ClientServiceDeleteUserTestWB.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientServiceDeleteUserTestWB.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
    }

    /**
     * Test of deleteUser method, of class ClientService.
     */
    /**
     * Testiramo deleteUser metodu ClientService-a (pokrivanje DU putanja)
     * 
     * metoda prima:
     * 
     * - klijenta
     * 
     * metoda vraca:
     * 
     * - true ukoliko prosledjeni klijent postoji u bazi, u suprotnom false
     */
    @Test
    public void testCSDeleteUserDUPutanjeDC0U1() {
        System.out.println("testCSDeleteUserDUPutanje DC0U1 - D c: 0, U: 1");
        
        ArrayList<Client> clients = ClientServiceDeleteUserTestWB.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        boolean expResult = true;
        boolean result = ClientServiceDeleteUserTestWB.instance.deleteUser(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCSDeleteUserDUPutanjeDC0U3() {
        System.out.println("testCSDeleteUserDUPutanje DC0U1 - D c: 0, U: 3");
        
        ArrayList<Client> clients = ClientServiceDeleteUserTestWB.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        boolean expResult = true;
        boolean result = ClientServiceDeleteUserTestWB.instance.deleteUser(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCSDeleteUserDUPutanjeDX1U2() {
        System.out.println("testCSDeleteUserDUPutanjeDX1U2 - D x: 1, U: 2");
        
//        klijent ne postoji
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = false;
        boolean result = ClientServiceDeleteUserTestWB.instance.deleteUser(object);
        assertEquals(expResult, result);
    }    
}
