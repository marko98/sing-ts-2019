/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import java.util.Arrays;
import service.*;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class ClientServiceLoginTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    private String username, password, message;
    private boolean shouldReturnNull;
    
    public ClientServiceLoginTest(String username, String password, boolean shouldReturnNull, String message) {
        this.username = username;
        this.password = password;
        this.shouldReturnNull = shouldReturnNull;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceLoginTest.clientDAO = new ClientDAO();
        ClientServiceLoginTest.instance = new ClientService();
        
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientServiceLoginTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
        
        object = new Client();
        object.setName("David");
        object.setUsername("david");
        object.setPassword("david");
        
        expResult = true;
        result = ClientServiceLoginTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<Client> clients = ClientServiceLoginTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientServiceLoginTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ClientServiceLoginTest.clientDAO = null;
        ClientServiceLoginTest.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
            {"marko", "marko", false, "C0C2P0 - korisnicko ime postoji i lozinka se poklapa, ne ocekujemo null"},
            {"david", "marko", true, "C1C2P1 - korisnicko ime ne postoji i lozinka se poklapa, ocekujemo null"},
            {"marko", "david", true, "C0C3P1 - korisnicko ime postoji i lozinka se ne poklapa, ocekujemo null"},
            {"david", "david", true, "C1C3P1 - korisnicko ime ne postoji i lozinka se ne poklapa, ocekujemo null"},
                
            {"", "marko", true, "C4C2P1 - korisnicko ime je prazno i lozinka se poklapa, ocekujemo null"},
            {"", "marko", true, "C4C2P1 - korisnicko ime je prazno i lozinka se poklapa, ocekujemo null"},
            {"", "david", true, "C4C3P1 - korisnicko ime je prazno i lozinka se ne poklapa, ocekujemo null"},
            {"", "david", true, "C4C3P1 - korisnicko ime je prazno i lozinka se ne poklapa, ocekujemo null"},
                
            {"marko", "", true, "C0C5P1 - korisnicko ime postoji i lozinka je prazna, ocekujemo null"},
            {"david", "", true, "C1C5P1 - korisnicko ime ne postoji i lozinka je prazna, ocekujemo null"},
            
            {"", "", true, "C4C5P1 - korisnicko ime je prazno i lozinka je prazna, ocekujemo null"},
        });
    }

    /**
     * Test of login method, of class ClientService.
     */
    /**
     * Testiramo login metodu ClientService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima korisničko ime i lozinku korisnika
     * 
     * metoda vraca:
     * 
     * - ukoliko se ne pronađe nijedan korisnik, vraća se null
     */
    @Test
    public void testCSLoginTOUPG() {
        System.out.println("testCSLoginTOUPG " + this.message);
        
        Client result = ClientServiceLoginTest.instance.login(this.username, this.username);
        
        if(this.shouldReturnNull){
            assertEquals(null, result);
        } else {
            assertNotEquals(null, result);
        }  
    }
    
}
