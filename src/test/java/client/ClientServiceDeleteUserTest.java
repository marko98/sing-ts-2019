/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import service.*;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ClientServiceDeleteUserTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    public ClientServiceDeleteUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceDeleteUserTest.clientDAO = new ClientDAO();
        ClientServiceDeleteUserTest.instance = new ClientService();
        
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientServiceDeleteUserTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<Client> clients = ClientServiceDeleteUserTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientServiceDeleteUserTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ClientServiceDeleteUserTest.clientDAO = null;
        ClientServiceDeleteUserTest.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of deleteUser method, of class ClientService.
     */
    /**
     * Testiramo deleteUser metodu ClientService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - klijenta
     * 
     * metoda vraca:
     * 
     * - true ukoliko prosledjeni klijent postoji u bazi, u suprotnom false
     */
    @Test
    public void testCSDeleteTOUPGC0P0() {
        System.out.println("testCSDeleteTOUPG C0P0 - prosledjeni klijent postoji, ocekujemo true");
        
        ArrayList<Client> clients = ClientServiceDeleteUserTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        boolean expResult = true;
        boolean result = ClientServiceDeleteUserTest.instance.deleteUser(onlyOne);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCSDeleteTOUPGC1P1() {
        System.out.println("testCSDeleteTOUPG C1P1 - prosledjeni klijent ne postoji, ocekujemo false");
        
        Client object = new Client();
        object.setName("Maja");
        object.setUsername("maja");
        object.setPassword("maja");
        
        boolean expResult = false;
        boolean result = ClientServiceDeleteUserTest.instance.deleteUser(object);
        assertEquals(expResult, result);
    }
    
}
