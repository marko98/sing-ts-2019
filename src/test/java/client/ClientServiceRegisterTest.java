/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import service.*;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class ClientServiceRegisterTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    private String name, username, password, message;
    private boolean expectedResult;
    
    public ClientServiceRegisterTest(String name, String username, String password, String message, boolean expectedResult) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceRegisterTest.clientDAO = new ClientDAO();
        ClientServiceRegisterTest.instance = new ClientService();
    }
    
    @AfterClass
    public static void tearDownClass() {       
        DatabaseConnection.close();
        ClientServiceRegisterTest.clientDAO = null;
        ClientServiceRegisterTest.instance = null;
    }
    
    @Before
    public void setUp() {
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientServiceRegisterTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
        
        object = new Client();
        object.setName("David");
        object.setUsername("david");
        object.setPassword("david");
        
        expResult = true;
        result = ClientServiceRegisterTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Client> clients = ClientServiceRegisterTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientServiceRegisterTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            String name, String username, String password, String message, boolean expectedResult
            {"", "maja", "", "C1C2C4C7P1 - korisnicko ime nije prazno, ime je prazno, lozinka je prazna, korisnicko ime ne postoji u bazi", false},
            {"Maja", "", "", "C0C3C4C7P1 - korisnicko ime je prazno, ime nije prazno, lozinka je prazna, korisnicko ime ne postoji u bazi", false},
            {"Maja", "maja", "", "C1C3C4C7P1 - korisnicko ime nije prazno, ime nije prazno, lozinka je prazna, korisnicko ime ne postoji u bazi", false},
            {"", "", "", "C0C2C4C7P1 - korisnicko ime je prazno, ime je prazno, lozinka je prazna, korisnicko ime ne postoji u bazi", false},
            {"", "maja", "maja", "C1C2C5C7P1 - korisnicko ime nije prazno, ime je prazno, lozinka nije prazna, korisnicko ime ne postoji u bazi", false},
            {"", "", "maja", "C0C2C5C7P1 - korisnicko ime je prazno, ime je prazno, lozinka nije prazna, korisnicko ime ne postoji u bazi", false},
            {"Maja", "", "maja", "C0C3C5C7P1 - korisnicko ime je prazno, ime nije prazno, lozinka nije prazna, korisnicko ime ne postoji u bazi", false},
            {"Maja", "maja", "maja", "C1C3C5C7P0 - korisnicko ime nije prazno, ime nije prazno, lozinka nije prazna, korisnicko ime ne postoji u bazi", true},
            {"David", "david", "", "C1C2C4C6P1 - korisnicko ime nije prazno, ime je prazno, lozinka je prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"David", "", "", "C0C3C4C6P1 - korisnicko ime je prazno, ime nije prazno, lozinka je prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"David", "david", "", "C1C3C4C6P1 - korisnicko ime nije prazno, ime nije prazno, lozinka je prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"", "", "", "C0C2C4C6P1 - korisnicko ime je prazno, ime je prazno, lozinka je prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"David", "david", "david", "C1C2C5C6P1 - korisnicko ime nije prazno, ime je prazno, lozinka nije prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"", "", "david", "C0C2C5C6P1 - korisnicko ime je prazno, ime je prazno, lozinka nije prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"David", "", "david", "C0C3C5C6P1 - korisnicko ime je prazno, ime nije prazno, lozinka nije prazna, korisnicko ime vec postoji u bazi", false}, //korisnicko ime vec postoji
            {"David", "david", "david", "C1C3C5C6P1 - korisnicko ime nije prazno, ime nije prazno, lozinka nije prazna, korisnicko ime vec postoji u bazi", false} //korisnicko ime vec postoji
        });
    }
    
    /**
     * Test of register method, of class ClientService.
     */
    /**
     * Testiramo register metodu ClientService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima ime, korisničko ime i lozinku korisnika
     * 
     * metoda vraca:
     * 
     * - ukoliko ni jedno polje nije prazno i ne postoji zeljeno korisnicko ime 
     * u bazi, vraca se true, u suprotnom false
     */
    @Test
    public void testCSRegisterTOUPG() {
        System.out.println("testCSRegisterTOUPG " + this.message);
        
        boolean expResult = this.expectedResult;
        boolean result = ClientServiceRegisterTest.instance.register(this.name, this.username, this.password);
        assertEquals(expResult, result);
        
        ArrayList<Client> clients = ClientServiceRegisterTest.clientDAO.getAll();
        int count = clients.size();
        if(result == true){
            System.out.println("U bazu nije dodat klijent, trebao bi biti");
            assertEquals(3, count);
        } else {
            System.out.println("U bazu je dodat klijent, ne bi trebao biti");
            assertEquals(2, count);
        }
    }
}
