/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import db.ClientDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import service.*;
import model.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ClientServiceUpdateInfoTest {
    private static ClientDAO clientDAO;
    private static ClientService instance;
    
    public ClientServiceUpdateInfoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ClientServiceUpdateInfoTest.clientDAO = new ClientDAO();
        ClientServiceUpdateInfoTest.instance = new ClientService();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        ClientServiceUpdateInfoTest.clientDAO = null;
        ClientServiceUpdateInfoTest.instance = null;
    }
    
    @Before
    public void setUp() {
        Client object = new Client();
        object.setName("Marko");
        object.setUsername("marko");
        object.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ClientServiceUpdateInfoTest.clientDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Client> clients = ClientServiceUpdateInfoTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ClientServiceUpdateInfoTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
    }

    /**
     * Test of updateInfo method, of class ClientService.
     */
    /**
     * Testiramo updateInfo metodu ClientService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - klijenta, ime, staru sifru i novu sifru
     * 
     * metoda vraca:
     * 
     * - ne vraca nista, vec ukoliko prosledjeni korisnik postoji i i login bude uspesan(stara sifra tacna)
     * korisniku se update-uje novo ime i lozinka
     */
    @Test
    public void testCSUpdateInfoTOUPGC0C2C5P0() {
        System.out.println("testCSUpdateInfoTOUPG C0C2C5P0 - klijent postoji u bazi, old password je tacna, novo ime nije prazno, ocekujemo izmene nad klijentom");
        String name = "David";
        String oldPassword = "marko";
        String password = "david";
        
        ArrayList<Client> clients = ClientServiceUpdateInfoTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        ClientServiceUpdateInfoTest.instance.updateInfo(onlyOne, name, oldPassword, password);
        
        Client updatedClient = ClientServiceUpdateInfoTest.clientDAO.getAll().get(0);
        assertEquals(name, updatedClient.getName());
        assertEquals(password, updatedClient.getPassword());
    }
    
    @Test
    public void testCSUpdateInfoTOUPGC0C3C5P1() {
        System.out.println("testCSUpdateInfoTOUPG C0C3C5P1 - klijent postoji u bazi, old password nije tacna, novo ime nije prazno, ne ocekujemo izmene nad klijentom");
        String name = "David";
        String oldPassword = "david";
        String password = "david";
        
        ArrayList<Client> clients = ClientServiceUpdateInfoTest.clientDAO.getAll();
        Client onlyOne = clients.get(0);
        
        ClientServiceUpdateInfoTest.instance.updateInfo(onlyOne, name, oldPassword, password);
        
        Client updatedClient = ClientServiceUpdateInfoTest.clientDAO.getAll().get(0);
        assertNotEquals(name, updatedClient.getName());
        assertNotEquals(password, updatedClient.getPassword());
    }
    
    @Test
    public void testCSUpdateInfoTOUPGC1C2C5P1() {
        System.out.println("testCSUpdateInfoTOUPG C1C2C5P1 - klijent ne postoji u bazi, old password je tacna, novo ime nije prazno, ne ocekujemo izmene nad klijentom");
        Client c = new Client();
        String name = "David";
        String oldPassword = "marko";
        String password = "david";
        
        ClientServiceUpdateInfoTest.instance.updateInfo(c, name, oldPassword, password);
        
        Client updatedClient = ClientServiceUpdateInfoTest.clientDAO.getAll().get(0);
        assertNotEquals(name, updatedClient.getName());
        assertNotEquals(password, updatedClient.getPassword());
    }
    
    @Test
    public void testCSUpdateInfoTOUPGC1C3C5P1() {
        System.out.println("testCSUpdateInfoTOUPG C1C3C5P1 - klijent ne postoji u bazi, old password nije tacna, novo ime nije prazno, ne ocekujemo izmene nad klijentom");
        Client c = new Client();
        String name = "David";
        String oldPassword = "david";
        String password = "david";
        
        ClientServiceUpdateInfoTest.instance.updateInfo(c, name, oldPassword, password);
        
        Client updatedClient = ClientServiceUpdateInfoTest.clientDAO.getAll().get(0);
        assertNotEquals(name, updatedClient.getName());
        assertNotEquals(password, updatedClient.getPassword());
    }
    
    @Test
    public void testCSUpdateInfoTOUPGC1C2C4P1() {
        System.out.println("testCSUpdateInfoTOUPG C1C2C4P1 - klijent ne postoji u bazi, old password je tacna, novo ime je prazno, ne ocekujemo izmene nad klijentom");
        Client c = new Client();
        String name = "";
        String oldPassword = "marko";
        String password = "david";
        
        ClientServiceUpdateInfoTest.instance.updateInfo(c, name, oldPassword, password);
        
        Client updatedClient = ClientServiceUpdateInfoTest.clientDAO.getAll().get(0);
        assertNotEquals(name, updatedClient.getName());
        assertNotEquals(password, updatedClient.getPassword());
    }
    
}
