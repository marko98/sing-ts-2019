/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactionService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import java.sql.Date;
import java.util.ArrayList;
import model.DeliveryService;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import static org.mockito.Mockito.*;
import org.mockito.InOrder;
import service.PromotionService;
import service.ShopItemService;
import service.TransactionService;

/**
 *
 * @author marko
 */
public class TransactionServiceCalculatePriceTest {
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ShopItemDAO shopItemDAO;
    
    public TransactionServiceCalculatePriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        TransactionServiceCalculatePriceTest.shopItemService = new ShopItemService();
        TransactionServiceCalculatePriceTest.instance = new TransactionService(TransactionServiceCalculatePriceTest.shopItemService);
        TransactionServiceCalculatePriceTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionServiceCalculatePriceTest.shopItemDAO = new ShopItemDAO();
        
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(100);
        
        boolean expResult = true;
        boolean result = TransactionServiceCalculatePriceTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
        
//        napravimo jednog dostavljaca
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = TransactionServiceCalculatePriceTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCalculatePriceTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCalculatePriceTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        TransactionServiceCalculatePriceTest.shopItemService = null;
        TransactionServiceCalculatePriceTest.instance = null;
        TransactionServiceCalculatePriceTest.deliveryServiceDAO = null;
        TransactionServiceCalculatePriceTest.shopItemDAO = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculatePrice method, of class TransactionService.
     */
    @Test
    public void testTSCalculatePriceNoHoliday() {
        System.out.println("testCalculatePriceNoHoliday");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem onlyOneShopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOneDeliveryServices = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = onlyOneDeliveryServices.getStartingPrice() + onlyOneShopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*onlyOneDeliveryServices.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da nije holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }
        

//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
//        zadamo da nije holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(false);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(onlyOneShopItem, distance, amount, onlyOneDeliveryServices);
        
        assertEquals(expectedPrice, result, 0.0);
        
        InOrder promotionServiceOrderNoHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateSpecialPromotions();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).checkHolidays(now);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateDiscount();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, never()).calculateSeasonalPromotions(now);
    } 
    
    @Test
    public void testTSCalculatePriceHoliday() {
        System.out.println("testCalculatePriceHoliday");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem onlyOneShopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOneDeliveryServices = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = onlyOneDeliveryServices.getStartingPrice() + onlyOneShopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*onlyOneDeliveryServices.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da je holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(true);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }
        

//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
//        zadamo da je holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(onlyOneShopItem, distance, amount, onlyOneDeliveryServices);
        
        assertEquals(expectedPrice, result, 0.0);

        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();
    } 
    
    @Test
    public void testTSCalculatePriceNoHolidayOrder() {
        System.out.println("testTSCalculatePriceNoHolidayOrder");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem onlyOneShopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOneDeliveryServices = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = onlyOneDeliveryServices.getStartingPrice() + onlyOneShopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*onlyOneDeliveryServices.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da nije holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(false);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }
        

//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
//        zadamo da nije holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(false);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(onlyOneShopItem, distance, amount, onlyOneDeliveryServices);
        
        InOrder promotionServiceOrderNoHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateSpecialPromotions();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).checkHolidays(now);
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, times(1)).calculateDiscount();
        promotionServiceOrderNoHoliday.verify(promotionServiceSpy2, never()).calculateSeasonalPromotions(now);
    } 
    
    @Test
    public void testTSCalculatePriceHolidayOrder() {
        System.out.println("testTSCalculatePriceHolidayOrder");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem onlyOneShopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOneDeliveryServices = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = onlyOneDeliveryServices.getStartingPrice() + onlyOneShopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*onlyOneDeliveryServices.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da je holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(true);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }

//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
//        zadamo da je holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(onlyOneShopItem, distance, amount, onlyOneDeliveryServices);

        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();
    }
    
    @Test
    public void testTSCalculatePriceShopItemDoesntExistHoliday() {
        System.out.println("testCalculatePriceShopItemDoesntExistHoliday");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem shopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOneDeliveryServices = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = onlyOneDeliveryServices.getStartingPrice() + shopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*onlyOneDeliveryServices.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da je holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(true);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }
        
//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
        shopItem = null;
        
//        zadamo da je holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(shopItem, distance, amount, onlyOneDeliveryServices);
        
        assertEquals(expectedPrice, result, 0.0);

        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();
    } 
    
    @Test
    public void testTSCalculatePriceDeliveryServiceDoesntExistHoliday() {
        System.out.println("testCalculatePriceDeliveryServiceDoesntExistHoliday");
        
        ArrayList<ShopItem> shopItems = TransactionServiceCalculatePriceTest.shopItemDAO.getAll();
        ShopItem onlyOneShopItem = shopItems.get(0);
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCalculatePriceTest.deliveryServiceDAO.getAll();
        DeliveryService deliveryService = deliveryServices.get(0);
        
        float distance = 100; // 100km
        int amount = 100;
        Date now = new Date(System.currentTimeMillis());
        
//        racunanje cene
//        1)
        float expectedPrice = deliveryService.getStartingPrice() + onlyOneShopItem.getPrice();
//        2)
        if(amount > 20 && amount < 50){
            expectedPrice = (float) (expectedPrice * 0.9);
        } else if(amount >= 50){
            expectedPrice = (float) (expectedPrice * 0.8);
        }
//        3)
        expectedPrice -= 350/distance*20;
//        4)
        expectedPrice += distance*100*deliveryService.getPricePerKilometer();
//        primena promocija ukoliko postoje
        PromotionService promotionServiceSpy1 = spy(PromotionService.class);
//        zadamo da je holiday
        when(promotionServiceSpy1.checkHolidays(now)).thenReturn(true);
        float discount = promotionServiceSpy1.applyPromotions(expectedPrice, now);
        if(discount > expectedPrice/2){
//        pass
        } else {
            expectedPrice -= discount;
        }

//        setujemo spy-a
        PromotionService promotionServiceSpy2 = spy(PromotionService.class);
        TransactionServiceCalculatePriceTest.instance.setPromotionService(promotionServiceSpy2);
        
        deliveryService = null;
        
//        zadamo da je holiday
        when(promotionServiceSpy2.checkHolidays(now)).thenReturn(true);
        float result = TransactionServiceCalculatePriceTest.instance.calculatePrice(onlyOneShopItem, distance, amount, deliveryService);
        
        assertEquals(expectedPrice, result, 0.0);

        InOrder promotionServiceOrderIsHoliday = inOrder(promotionServiceSpy2);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSpecialPromotions();
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).checkHolidays(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateSeasonalPromotions(now);
        promotionServiceOrderIsHoliday.verify(promotionServiceSpy2).calculateDiscount();
    } 
    
}
