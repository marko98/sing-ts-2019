/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactionService;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import db.TransactionDAOTest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.TransactionService;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import service.ShopItemService;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class TransactionServiceCompleteTransactionTest {
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static TransactionDAO transactionDAO;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ShopItemDAO shopItemDAO;
    private static ClientDAO clientDAO;
    
    private int kupljenaKolicina;
    private float udaljenost;
    private boolean klijentPostoji, dostavljacPostoji, proizvodPostoji, ocekujemoIzuzetak;
    private String message;
    
    public TransactionServiceCompleteTransactionTest(boolean klijentPostoji, boolean dostavljacPostoji, boolean proizvodPostoji, 
            int kupljenaKolicina, float udaljenost, boolean ocekujemoIzuzetak, String message) {
        this.klijentPostoji = klijentPostoji;
        this.dostavljacPostoji = dostavljacPostoji;
        this.proizvodPostoji = proizvodPostoji;
        this.kupljenaKolicina = kupljenaKolicina;
        this.udaljenost = udaljenost;
        this.ocekujemoIzuzetak = ocekujemoIzuzetak;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        TransactionServiceCompleteTransactionTest.shopItemService = new ShopItemService();
        TransactionServiceCompleteTransactionTest.instance = new TransactionService(TransactionServiceCompleteTransactionTest.shopItemService);
        TransactionServiceCompleteTransactionTest.transactionDAO = new TransactionDAO();
        TransactionServiceCompleteTransactionTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionServiceCompleteTransactionTest.shopItemDAO = new ShopItemDAO();
        TransactionServiceCompleteTransactionTest.clientDAO = new ClientDAO();
        
//        napravimo jednog klijenta
        Client client = new Client();
        client.setName("Marko");
        client.setUsername("marko");
        client.setPassword("marko");
        
        boolean expResult = true;
        boolean result = TransactionServiceCompleteTransactionTest.clientDAO.insertOne(client);
        assertEquals(expResult, result);
       
//        napravimo jednog dostavljaca
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = TransactionServiceCompleteTransactionTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<Transaction> transactions = TransactionServiceCompleteTransactionTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = TransactionServiceCompleteTransactionTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceCompleteTransactionTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        }); 
               
        ArrayList<Client> clients = TransactionServiceCompleteTransactionTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        TransactionServiceCompleteTransactionTest.shopItemService = null;
        TransactionServiceCompleteTransactionTest.instance = null;
        TransactionServiceCompleteTransactionTest.transactionDAO = null;
        TransactionServiceCompleteTransactionTest.deliveryServiceDAO = null;
        TransactionServiceCompleteTransactionTest.shopItemDAO = null;
        TransactionServiceCompleteTransactionTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(100);
        
        boolean expResult = true;
        boolean result = TransactionServiceCompleteTransactionTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Transaction> transactions = TransactionServiceCompleteTransactionTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = TransactionServiceCompleteTransactionTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionServiceCompleteTransactionTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            boolean klijentPostoji, boolean dostavljacPostoji, boolean proizvodPostoji, int kupljenaKolicina, float udaljenost, boolean ocekujemoIzuzetak, String message
            {true, true, true, 100, 10, false, "C0C2C4C6C10P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 ima toliko na lageru, udaljenost > 0, ne ocekujemo exception"},
            {true, true, true, 100, 0, true, "C0C2C4C6C11P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 ima toliko na lageru, udaljenost = 0, ocekujemo exception"},
            {true, true, true, 100, -10, true, "C0C2C4C6C12P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 ima toliko na lageru, udaljenost < 0, ocekujemo exception"},
            
            {true, true, true, 300, 10, false, "C0C2C4C7C10P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 nema toliko na lageru, udaljenost > 0, ne ocekujemo exception"},
            {true, true, true, 300, 0, true, "C0C2C4C7C11P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 nema toliko na lageru, udaljenost = 0, ocekujemo exception"},
            {true, true, true, 300, -10, true, "C0C2C4C7C12P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina > 0 nema toliko na lageru, udaljenost < 0, ocekujemo exception"},
            
            {true, true, true, 0, 10, true, "C0C2C4C8C10P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina = 0, udaljenost > 0, ocekujemo exception"},
            {true, true, true, 0, 0, true, "C0C2C4C8C11P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina = 0, udaljenost = 0, ocekujemo exception"},
            {true, true, true, 0, -10, true, "C0C2C4C8C12P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina = 0, udaljenost < 0, ocekujemo exception"},
            
            {true, true, true, -100, 10, true, "C0C2C4C9C10P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina < 0, udaljenost > 0, ocekujemo exception"},
            {true, true, true, -100, 0, true, "C0C2C4C9C11P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina < 0, udaljenost = 0, ocekujemo exception"},
            {true, true, true, -100, -10, true, "C0C2C4C9C12P0 - klijent postoji, dostavljac postoji, proizvod postoji, kupljenaKolicina < 0, udaljenost < 0, ocekujemo exception"},
        
            {false, false, false, 100, 10, true, "C1C3C5C6C10P0 - klijent ne postoji, dostavljac ne postoji, proizvod ne postoji, kupljenaKolicina > 0 ima toliko na lageru, udaljenost > 0, ocekujemo exception"},
        });
    }

    /**
     * Test of completeTransaction method, of class TransactionService.
     */
    /**
     * Testiramo completeTransaction metodu TransactionService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - korisnika, dostavljaca, prozivod, kupljenuKolicinu i udaljenost
     * 
     * metoda vraca:
     * 
     * - ne vraca nista, vec ukoliko su prosledjeni podaci validni
     *   dolazi do kreiranja transakcije i pamcenja nje u bazu, poziva kupovine proizvoda(umanjenje kolicine na lageru), a u suprotnom
     *   ocekujemo IllegalArgumentException
     */
    @Test
    public void testTSCompleteTransactionTOUPG() {
        System.out.println("testTSCompleteTransactionTOUPG " + this.message);
        
        Client client = null;
        if(this.klijentPostoji){
            ArrayList<Client> clients = TransactionServiceCompleteTransactionTest.clientDAO.getAll();
            client = clients.get(0);
        } else {
            client = new Client();
            client.setName("Marko");
            client.setUsername("marko");
            client.setPassword("marko");
        }
        
        DeliveryService deliveryService = null;
        if(this.dostavljacPostoji){
            ArrayList<DeliveryService> deliveryServices = TransactionServiceCompleteTransactionTest.deliveryServiceDAO.getAll();
            deliveryService = deliveryServices.get(0);
        } else {
            deliveryService = new DeliveryService();
            deliveryService.setName("Amazaon"); // UPS
            deliveryService.setStartingPrice(1000);
            deliveryService.setPricePerKilometer(200);
        }
        
        ShopItem shopItem = null;
        if(this.proizvodPostoji){
            ArrayList<ShopItem> shopItems = TransactionServiceCompleteTransactionTest.shopItemDAO.getAll();
            shopItem = shopItems.get(0);
        } else {
            shopItem = new ShopItem();
            shopItem.setName("Muski sampon za kosu");
            shopItem.setAmount(200);
            shopItem.setPrice(100);
        }
        
        boolean catchedIllegalArgumentException = false;
        try {
            TransactionServiceCompleteTransactionTest.instance.completeTransaction(client, deliveryService, shopItem, this.kupljenaKolicina, this.udaljenost);
        } catch (IllegalArgumentException e) {
            catchedIllegalArgumentException = true;
        }
        
        assertEquals(this.ocekujemoIzuzetak, catchedIllegalArgumentException);
        
        if(!this.ocekujemoIzuzetak){
            ArrayList<Transaction> transactions = TransactionServiceCompleteTransactionTest.transactionDAO.getAll();
            assertEquals(1, transactions.size());
            
            ArrayList<ShopItem> shopItems = TransactionServiceCompleteTransactionTest.shopItemDAO.getAll();
            ShopItem onlyOne = shopItems.get(0);
            
            if(this.kupljenaKolicina > shopItem.getAmount()){
                assertEquals(200, onlyOne.getAmount());
            } else {
                assertEquals(200-this.kupljenaKolicina, onlyOne.getAmount());
            }
        }
    }
}
