/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactionService;

import shopItem.ShopItemServiceCheckIfPopularTest;
import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import db.TransactionDAOTest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.TransactionService;
import static org.junit.Assert.*;
import service.ShopItemService;

/**
 *
 * @author marko
 */
public class TransactionServiceGetRecentTransactionsTest {
    private static TransactionService instance;
    private static ShopItemService shopItemService;
    private static TransactionDAO transactionDAO;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ShopItemDAO shopItemDAO;
    private static ClientDAO clientDAO;
    
    public TransactionServiceGetRecentTransactionsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        TransactionServiceGetRecentTransactionsTest.shopItemService = new ShopItemService();
        TransactionServiceGetRecentTransactionsTest.instance = new TransactionService(TransactionServiceGetRecentTransactionsTest.shopItemService);
        
        TransactionServiceGetRecentTransactionsTest.transactionDAO = new TransactionDAO();
        TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO = new DeliveryServiceDAO();
        TransactionServiceGetRecentTransactionsTest.shopItemDAO = new ShopItemDAO();
        TransactionServiceGetRecentTransactionsTest.clientDAO = new ClientDAO();
        
//        napravimo jednog klijenta
        Client client = new Client();
        client.setName("Marko");
        client.setUsername("marko");
        client.setPassword("marko");
        
        boolean expResult = true;
        boolean result = TransactionServiceGetRecentTransactionsTest.clientDAO.insertOne(client);
        assertEquals(expResult, result);
       
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<Transaction> transactions = TransactionServiceGetRecentTransactionsTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = TransactionServiceGetRecentTransactionsTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<DeliveryService> deliveryServices = TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<Client> clients = TransactionServiceGetRecentTransactionsTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        TransactionServiceGetRecentTransactionsTest.shopItemService = null;
        TransactionServiceGetRecentTransactionsTest.instance = null;
        TransactionServiceGetRecentTransactionsTest.transactionDAO = null;
        TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO = null;
        TransactionServiceGetRecentTransactionsTest.shopItemDAO = null;
        TransactionServiceGetRecentTransactionsTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(100);
        
        boolean expResult = true;
        boolean result = TransactionServiceGetRecentTransactionsTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<Transaction> transactions = TransactionServiceGetRecentTransactionsTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = TransactionServiceGetRecentTransactionsTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = TransactionServiceGetRecentTransactionsTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
    }

    /**
     * Test of getRecentTransactions method, of class TransactionService.
     */
    @Test
    public void testTSGetRecentTransactionsGVCreated40DaysAgo() {
        System.out.println("testTSGetRecentTransactionsGVCreated40DaysAgo");
        
        LocalDate localDate = LocalDate.now().minusDays(40);
        Date date = Date.valueOf(localDate);
//        kreiramo transkaciju
        createTransaction(date);
        
        ArrayList<Transaction> result = TransactionServiceGetRecentTransactionsTest.instance.getRecentTransactions();
        assertEquals(0, result.size());
    }
    
    @Test
    public void testTSGetRecentTransactionsGVCreated30DaysAgo() {
        System.out.println("testTSGetRecentTransactionsGVCreated30DaysAgo");
        
        LocalDate localDate = LocalDate.now().minusDays(30);
        Date date = Date.valueOf(localDate);
//        kreiramo transkaciju
        createTransaction(date);
        
        ArrayList<Transaction> result = TransactionServiceGetRecentTransactionsTest.instance.getRecentTransactions();
        assertEquals(1, result.size());
    }
    
    @Test
    public void testTSGetRecentTransactionsGVCreated10DaysAgo() {
        System.out.println("testTSGetRecentTransactionsGVCreated10DaysAgo");
        
        LocalDate localDate = LocalDate.now().minusDays(10);
        Date date = Date.valueOf(localDate);
//        kreiramo transkaciju
        createTransaction(date);
        
        ArrayList<Transaction> result = TransactionServiceGetRecentTransactionsTest.instance.getRecentTransactions();
        assertEquals(1, result.size());
    }
    
    @Test
    public void testTSGetRecentTransactionsGVNoCreatedTransaction() {
        System.out.println("testTSGetRecentTransactionsGVNoCreatedTransaction");
        
        ArrayList<Transaction> result = TransactionServiceGetRecentTransactionsTest.instance.getRecentTransactions();
        assertEquals(0, result.size());
    }
    
    public void createTransaction(Date date){
        ArrayList<Client> clients = TransactionServiceGetRecentTransactionsTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = TransactionServiceGetRecentTransactionsTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = TransactionServiceGetRecentTransactionsTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
//        napravimo jednu transakcije
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(100); // ukupno ima 200 komada
        transaction.setDate(date);
//        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = TransactionServiceGetRecentTransactionsTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
}
