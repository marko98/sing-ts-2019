/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class DeliveryServiceServiceRegisterTest {
    private static DeliveryServiceService instance;
    private static DeliveryServiceDAO deliveryServiceDAO;
    
    private String ime, message;
    private float pocetnaCena, cenaPoKilometru;
    private boolean expectedResult;
    
    public DeliveryServiceServiceRegisterTest(String ime, float pocetnaCena, float cenaPoKilometru, boolean expectedResult, String message) {
        this.ime = ime;
        this.pocetnaCena = pocetnaCena;
        this.cenaPoKilometru = cenaPoKilometru;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        DeliveryServiceServiceRegisterTest.instance = new DeliveryServiceService();
        DeliveryServiceServiceRegisterTest.deliveryServiceDAO = new DeliveryServiceDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        DeliveryServiceServiceRegisterTest.instance = null;
        DeliveryServiceServiceRegisterTest.deliveryServiceDAO = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceRegisterTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = DeliveryServiceServiceRegisterTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//          String ime, float pocetnaCena, float cenaPoKilometru, boolean expectedResult, String message
            {"UPS", 1000, 100, true, "C0C2C5P0 - ime nije prazno, pocetnaCena > 0, cenaPoKilometru > 0, ocekujemo true"},
            {"UPS", 1000, 0, false, "C0C2C6P1 - ime nije prazno, pocetnaCena > 0, cenaPoKilometru = 0, ocekujemo false"},
            {"UPS", 1000, -100, false, "C0C2C7P1 - ime nije prazno, pocetnaCena > 0, cenaPoKilometru < 0, ocekujemo false"},
            
            {"UPS", 0, 100, false, "C0C3C5P0 - ime nije prazno, pocetnaCena = 0, cenaPoKilometru > 0, ocekujemo false"},
            {"UPS", 0, 0, false, "C0C3C6P1 - ime nije prazno, pocetnaCena = 0, cenaPoKilometru = 0, ocekujemo false"},
            {"UPS", 0, -100, false, "C0C3C7P1 - ime nije prazno, pocetnaCena = 0, cenaPoKilometru < 0, ocekujemo false"},
            
            {"UPS", -1000, 100, false, "C0C4C5P0 - ime nije prazno, pocetnaCena < 0, cenaPoKilometru > 0, ocekujemo false"},
            {"UPS", -1000, 0, false, "C0C4C6P1 - ime nije prazno, pocetnaCena < 0, cenaPoKilometru = 0, ocekujemo false"},
            {"UPS", -1000, -100, false, "C0C4C7P1 - ime nije prazno, pocetnaCena < 0, cenaPoKilometru < 0, ocekujemo false"},
            
            {"", 1000, 100, false, "C1C2C5P0 - ime je prazno, pocetnaCena > 0, cenaPoKilometru > 0, ocekujemo false"},
            {"", 1000, 0, false, "C1C2C6P1 - ime je prazno, pocetnaCena > 0, cenaPoKilometru = 0, ocekujemo false"},
            {"", 1000, -100, false, "C1C2C7P1 - ime je prazno, pocetnaCena > 0, cenaPoKilometru < 0, ocekujemo false"},
            
            {"", 0, 100, false, "C1C3C5P0 - ime je prazno, pocetnaCena = 0, cenaPoKilometru > 0, ocekujemo false"},
            {"", 0, 0, false, "C1C3C6P1 - ime je prazno, pocetnaCena = 0, cenaPoKilometru = 0, ocekujemo false"},
            {"", 0, -100, false, "C1C3C7P1 - ime je prazno, pocetnaCena = 0, cenaPoKilometru < 0, ocekujemo false"},
            
            {"", -1000, 100, false, "C1C4C5P0 - ime je prazno, pocetnaCena < 0, cenaPoKilometru > 0, ocekujemo false"},
            {"", -1000, 0, false, "C1C4C6P1 - ime je prazno, pocetnaCena < 0, cenaPoKilometru = 0, ocekujemo false"},
            {"", -1000, -100, false, "C1C4C7P1 - ime je prazno, pocetnaCena < 0, cenaPoKilometru < 0, ocekujemo false"},
        });
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    /**
     * Testiramo register metodu DeliveryServiceService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima ime, pocetnaCena, cenaPoKilometru
     * 
     * metoda vraca:
     * 
     * - ukoliko su podaci validni vraca se true, u suprotnom false
     * 
     * 
     * Granicne vrednosti i klase ekvivalencije
     * pocetnaCena, cenaPoKilometru moraju biti vece od 0!
     * 
     * legalne:
     *  pocetnaCena i cenaPoKilometru > 0
     * ilegalne:
     *  pocetnaCena i cenaPoKilometru =<0
     *
     */
    @Test
    public void testDSSRegisterTOUPGGVKE() {
        System.out.println("testDSSRegisterTOUPGGVKE " + this.message);
        String name = this.ime;
        float pricePerKilometer = this.cenaPoKilometru;
        float startingPrice = this.pocetnaCena;
        
        boolean expResult = this.expectedResult;
        boolean result = DeliveryServiceServiceRegisterTest.instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceRegisterTest.deliveryServiceDAO.getAll();
        int count = deliveryServices.size();
        if(result == true){
            System.out.println("U bazu nije dodat delivery service, trebao bi biti");
            assertEquals(1, count);
        } else {
            System.out.println("U bazu je dodat delivery service, ne bi trebao biti");
            assertEquals(0, count);
        }
    }
    
}
