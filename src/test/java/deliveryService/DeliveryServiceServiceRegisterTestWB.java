/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import service.DeliveryServiceService;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class DeliveryServiceServiceRegisterTestWB {
    private static DeliveryServiceService instance;
    private static DeliveryServiceDAO deliveryServiceDAO;
    
    private String ime, message;
    private float pocetnaCena, cenaPoKilometru;
    private boolean expectedResult;
    
    public DeliveryServiceServiceRegisterTestWB(String ime, float pocetnaCena, float cenaPoKilometru, boolean expectedResult, String message) {
        this.ime = ime;
        this.pocetnaCena = pocetnaCena;
        this.cenaPoKilometru = cenaPoKilometru;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        DeliveryServiceServiceRegisterTestWB.instance = new DeliveryServiceService();
        DeliveryServiceServiceRegisterTestWB.deliveryServiceDAO = new DeliveryServiceDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        DeliveryServiceServiceRegisterTestWB.instance = null;
        DeliveryServiceServiceRegisterTestWB.deliveryServiceDAO = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceRegisterTestWB.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = DeliveryServiceServiceRegisterTestWB.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//          String ime, float pocetnaCena, float cenaPoKilometru, boolean expectedResult, String message
            {"UPS", 0, 0, false, "D ime: 0, U: 1, ime nije prazno, pocetnaCena = 0, cenaPoKilometru = 0, ocekujemo false"},
            {"UPS", 1000, 200, true, "D ime: 0, U: 3, ime nije prazno, pocetnaCena > 0, cenaPoKilometru > 0, ocekujemo true"},
//            sa ova dva testa su pokriveni i slucajevi
            /**
             * D pocetnaCena: 0, U: 1
             * D pocetnaCena: 0, U: 3
             * 
             * D cenaPoKilometru: 0, U: 1
             * D cenaPoKilometru: 0, U: 3
             */
        });
    }

    /**
     * Test of register method, of class DeliveryServiceService.
     */
    /**
     * Testiramo register metodu DeliveryServiceService-a (pokrivanje DU putanje)
     * 
     * metoda prima:
     * 
     * - prima ime, pocetnaCena, cenaPoKilometru
     * 
     * metoda vraca:
     * 
     * - ukoliko su podaci validni vraca se true, u suprotnom false
     */
    @Test
    public void testDSSRegisterDUPutanje() {
        System.out.println("testDSSRegisterDUPutanje " + this.message);
        String name = this.ime;
        float pricePerKilometer = this.cenaPoKilometru;
        float startingPrice = this.pocetnaCena;
        
        boolean expResult = this.expectedResult;
        boolean result = DeliveryServiceServiceRegisterTestWB.instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceRegisterTestWB.deliveryServiceDAO.getAll();
        int count = deliveryServices.size();
        if(result == true){
            System.out.println("U bazu nije dodat delivery service, trebao bi biti");
            assertEquals(1, count);
        } else {
            System.out.println("U bazu je dodat delivery service, ne bi trebao biti");
            assertEquals(0, count);
        }
    }
    
}
