/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class DeliveryServiceServiceDeleteDeliveryServiceTest {
    private static DeliveryServiceService instance;
    private static DeliveryServiceDAO deliveryServiceDAO;
    
    public DeliveryServiceServiceDeleteDeliveryServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        DeliveryServiceServiceDeleteDeliveryServiceTest.instance = new DeliveryServiceService();
        DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO = new DeliveryServiceDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        DeliveryServiceServiceDeleteDeliveryServiceTest.instance = null;
        DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO = null;
    }
    
    @Before
    public void setUp() {
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        boolean expResult = true;
        boolean result = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
    }

    /**
     * Test of deleteDeliveryService method, of class DeliveryServiceService.
     */
    /**
     * Testiramo deleteDeliveryService metodu DeliveryServiceService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - DeliveryService
     * 
     * metoda vraca:
     * 
     * - true ukoliko DeliveryService postoji u bazi, u suprotnom false
     */
    @Test
    public void testDSSDeleteDeliveryServiceTOUPGC0P0() {
        System.out.println("testDSSDeleteDeliveryServiceTOUPG C0P0 - deliveryService exists");
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.getAll();
        DeliveryService onlyOne = deliveryServices.get(0);
        
        boolean expResult = true;
        boolean result = DeliveryServiceServiceDeleteDeliveryServiceTest.instance.deleteDeliveryService(onlyOne);
        assertEquals(expResult, result);
        
        deliveryServices = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.getAll();
        int count = deliveryServices.size();
        if(result == true){
            System.out.println("U bazi ne bi trebao da bude i jedan delivery service");
            assertEquals(0, count);
        } else {
            System.out.println("U bazi bi trebao da bude jedan delivery service");
            assertEquals(1, count);
        }
    }
    
    @Test
    public void testDSSDeleteDeliveryServiceTOUPGC1P1() {
        System.out.println("testDSSDeleteDeliveryServiceTOUPG C1P1 - delivery service doesn't exist");
        
        DeliveryService deliveryService = new DeliveryService();
        
        System.out.println("Moguce da je doslo do nekih promena u bazi kojih ne bi trebalo da bude");
        
        boolean expResult = false;
        boolean result = DeliveryServiceServiceDeleteDeliveryServiceTest.instance.deleteDeliveryService(deliveryService);
        assertEquals(expResult, result);
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceDeleteDeliveryServiceTest.deliveryServiceDAO.getAll();
        int count = deliveryServices.size();
        if(result == true){
            System.out.println("U bazi bi trebao da bude jedan delivery service");
            assertEquals(1, count);
        } else {
            System.out.println("U bazi bi trebalo da budu dva delivery service-a");
            assertEquals(2, count);
        }
    }
}
