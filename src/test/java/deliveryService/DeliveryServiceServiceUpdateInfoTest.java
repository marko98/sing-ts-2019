/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliveryService;

import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.DeliveryService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.DeliveryServiceService;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class DeliveryServiceServiceUpdateInfoTest {
    private static DeliveryServiceService instance;
    private static DeliveryServiceDAO deliveryServiceDAO;
    
    private String message, novoIme;
    private float novaPocetnaCena, novaCenaPoKm;
    private boolean existsInDatabase, expectedResult;
    
    public DeliveryServiceServiceUpdateInfoTest(boolean existsInDatabase, String novoIme, float novaPocetnaCena, float novaCenaPoKm, boolean expectedResult, String message) {
        this.novoIme = novoIme;
        this.novaPocetnaCena = novaPocetnaCena;
        this.novaCenaPoKm = novaCenaPoKm;
        this.expectedResult = expectedResult;
        this.message = message;
        this.existsInDatabase = existsInDatabase;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        DeliveryServiceServiceUpdateInfoTest.instance = new DeliveryServiceService();
        DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO = new DeliveryServiceDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        DeliveryServiceServiceUpdateInfoTest.instance = null;
        DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO = null;
    }
    
    @Before
    public void setUp() {
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        boolean expResult = true;
        boolean result = DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            boolean existsInDatabase, String novoIme, float novaPocetnaCena, float novaCenaPoKm, boolean expectedResult, String message
            {true, "UPS", 2000, 400, true, "C0C2C4C7P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je > 0, ocekujemo true"},
            {true, "UPS", 2000, 0, false, "C0C2C4C8P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "UPS", 2000, -400, false, "C0C2C4C9P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {true, "UPS", 0, 400, false, "C0C2C5C7P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je > 0, ocekujemo false"},
            {true, "UPS", 0, 0, false, "C0C2C5C8P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "UPS", 0, -400, false, "C0C2C5C9P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {true, "UPS", -2000, 400, false, "C0C2C6C7P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je > 0, ocekujemo false"},
            {true, "UPS", -2000, 0, false, "C0C2C6C8P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "UPS", -2000, -400, false, "C0C2C6C9P0 - deliveryService postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {true, "", 2000, 400, false, "C0C3C4C7P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je > 0, ocekujemo false"},
            {true, "", 2000, 0, false, "C0C3C4C8P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "", 2000, -400, false, "C0C3C4C9P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {true, "", 0, 400, false, "C0C3C5C7P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je > 0, ocekujemo false"},
            {true, "", 0, 0, false, "C0C3C5C8P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "", 0, -400, false, "C0C3C5C9P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {true, "", -2000, 400, false, "C0C3C6C7P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je > 0, ocekujemo false"},
            {true, "", -2000, 0, false, "C0C3C6C8P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je = 0, ocekujemo false"},
            {true, "", -2000, -400, false, "C0C3C6C9P0 - deliveryService postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "UPS", 2000, 400, false, "C1C2C4C7P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "UPS", 2000, 0, false, "C1C2C4C8P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "UPS", 2000, -400, false, "C1C2C4C9P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je > 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "UPS", 0, 400, false, "C1C2C5C7P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "UPS", 0, 0, false, "C1C2C5C8P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "UPS", 0, -400, false, "C1C2C5C9P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je = 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "UPS", -2000, 400, false, "C1C2C6C7P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "UPS", -2000, 0, false, "C1C2C6C8P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "UPS", -2000, -400, false, "C1C2C6C9P0 - deliveryService ne postoji u bazi, novoIme nije prazno, novaPocetnaCena je < 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "", 2000, 400, false, "C1C3C4C7P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "", 2000, 0, false, "C1C3C4C8P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "", 2000, -400, false, "C1C3C4C9P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je > 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "", 0, 400, false, "C1C3C5C7P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "", 0, 0, false, "C1C3C5C8P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "", 0, -400, false, "C1C3C5C9P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je = 0, novaCenaPoKm je < 0, ocekujemo false"},
            
            {false, "", -2000, 400, false, "C1C3C6C7P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je > 0, ocekujemo false"},
            {false, "", -2000, 0, false, "C1C3C6C8P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je = 0, ocekujemo false"},
            {false, "", -2000, -400, false, "C1C3C6C9P0 - deliveryService ne postoji u bazi, novoIme je prazno, novaPocetnaCena je < 0, novaCenaPoKm je < 0, ocekujemo false"}
        });
    }

    /**
     * Test of updateInfo method, of class DeliveryServiceService.
     */
    /**
     * Testiramo updateInfo metodu DeliveryServiceService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - DeliveryService, novoIme, novaPocetnaCena i novaCenaPoKm
     * 
     * metoda vraca:
     * 
     * - ne vraca nista, vec ukoliko prosledjeni deliveryService postoji i prosledjeni podaci su validni
     *   dolazi do izmene u bazi
     */
    @Test
    public void testDSSUpdateInfoTOUPG() {
        System.out.println("testDSSUpdateInfoTOUPG " + this.message);
        
        DeliveryService onlyOne = null;
        if(this.existsInDatabase){
            ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO.getAll();
            onlyOne = deliveryServices.get(0);
        } else {
            onlyOne = new DeliveryService();
            onlyOne.setName("UPS");
            onlyOne.setStartingPrice(2000);
            onlyOne.setPricePerKilometer(400);
        }
        
        String oldName = onlyOne.getName();
        float oldStartingPrice = onlyOne.getStartingPrice();
        float oldPricePerKilometer = onlyOne.getPricePerKilometer();
        
        String name = this.novoIme;
        float startingPrice = this.novaPocetnaCena;
        float pricePerKilometer = this.novaCenaPoKm;
        
        boolean result = DeliveryServiceServiceUpdateInfoTest.instance.updateInfo(onlyOne, name, startingPrice, pricePerKilometer);
        assertEquals(this.expectedResult, result);
        
        ArrayList<DeliveryService> deliveryServices = DeliveryServiceServiceUpdateInfoTest.deliveryServiceDAO.getAll();
        DeliveryService updatedOne = deliveryServices.get(0);
        if(result == true){
            System.out.println("Nisu svi podaci delivery service-a update-ovani kako treba");
            assertEquals(this.novoIme, updatedOne.getName());
            assertEquals(this.novaPocetnaCena, updatedOne.getStartingPrice(), 0.00);
            assertEquals(this.novaCenaPoKm, updatedOne.getPricePerKilometer(), 0.00);
        } else {
            
            if(this.existsInDatabase){
                System.out.println("Doslo je do update-a delivery service-a, a nije trebalo");
                assertEquals(oldName, updatedOne.getName());
                assertEquals(oldStartingPrice, updatedOne.getStartingPrice(), 0.00);
                assertEquals(oldPricePerKilometer, updatedOne.getPricePerKilometer(), 0.00);
            } else {
                System.out.println("Doslo je do update-a delivery service-a, a nije trebalo");
                assertEquals(oldName, onlyOne.getName());
                assertEquals(oldStartingPrice, onlyOne.getStartingPrice(), 0.00);
                assertEquals(oldPricePerKilometer, onlyOne.getPricePerKilometer(), 0.00);
            }
            
        }
    }
}
