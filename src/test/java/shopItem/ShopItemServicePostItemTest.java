/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.ShopItemDAO;
import db.DatabaseConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class ShopItemServicePostItemTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    
    private String name, message;
    private boolean expectedResult;
    private float price;
    private int amount;
    
    public ShopItemServicePostItemTest(String name, float price, int amount, boolean expectedResult, String message) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServicePostItemTest.shopItemDAO = new ShopItemDAO();
        ShopItemServicePostItemTest.instance = new ShopItemService();
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<ShopItem> shopItems = ShopItemServicePostItemTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServicePostItemTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ShopItemServicePostItemTest.shopItemDAO = null;
        ShopItemServicePostItemTest.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            String name, float price, int amount, boolean expectedResult, String message
            {"olovka", 50, 100, true, "C0C2C5P0 ime proizvoda nije prazno, cena je pozitivna, kolicina je pozitivna, ocekujemo true"},
            {"olovka", -50, 100, false, "C0C3C5P1 ime proizvoda nije prazno, cena je negativna, kolicina je pozitivna, ocekujemo false"},
            {"olovka", -50, -100, false, "C0C3C7P1 ime proizvoda nije prazno, cena je negativna, kolicina je negativna, ocekujemo false"},
            {"olovka", 50, -100, false, "C0C2C7P1 ime proizvoda nije prazno, cena je pozitivna, kolicina je negativna, ocekujemo false"},
            {"olovka", 50, 0, true, "C0C2C6P0 ime proizvoda nije prazno, cena je pozitivna, kolicina je 0, ocekujemo true"},
            {"olovka", -50, 0, false, "C0C3C6P1 ime proizvoda nije prazno, cena je negativna, kolicina je 0, ocekujemo false"},
            {"olovka", 0, 100, true, "C0C4C5P0 ime proizvoda nije prazno, cena je 0, kolicina je pozitivna, ocekujemo true"},
            {"olovka", 0, -100, false, "C0C4C7P1 ime proizvoda nije prazno, cena je 0, kolicina je negativna, ocekujemo false"},
            {"olovka", 0, 0, true, "C0C4C6P0 ime proizvoda nije prazno, cena je 0, kolicina je 0, ocekujemo true"},
            {"", 50, 100, false, "C1C2C5P1 ime proizvoda je prazno, cena je pozitivna, kolicina je pozitivna, ocekujemo false"},
            {"", -50, 100, false, "C1C3C5P1 ime proizvoda je prazno, cena je negativna, kolicina je pozitivna, ocekujemo false"},
            {"", -50, -100, false, "C1C3C7P1 ime proizvoda je prazno, cena je negativna, kolicina je negativna, ocekujemo false"},
            {"", 50, -100, false, "C1C2C7P1 ime proizvoda je prazno, cena je pozitivna, kolicina je negativna, ocekujemo false"},
            {"", 50, 0, false, "C1C2C6P1 ime proizvoda je prazno, cena je pozitivna, kolicina je 0, ocekujemo false"},
            {"", -50, 0, false, "C1C3C6P1 ime proizvoda je prazno, cena je negativna, kolicina je 0, ocekujemo false"},
            {"", 0, 100, false, "C1C4C5P1 ime proizvoda je prazno, cena je 0, kolicina je pozitivna, ocekujemo false"},
            {"", 0, -100, false, "C1C4C7P1 ime proizvoda je prazno, cena je 0, kolicina je negativna, ocekujemo false"},
        });
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */
    /**
     * Testiramo postItem metodu ShopItemService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima ime, cenu i kolicinu
     * 
     * metoda vraca:
     * 
     * - ukoliko ni jedno polje nije prazno i cena, niti kolicina nisu negativne, 
     * vraca se true, u suprotnom false
     */
    @Test
    public void testSISPostItemTOUPGGVKE() {
        System.out.println("testSISPostItemTOUPGGVKE " + this.message);
        
        boolean expResult = this.expectedResult;
        boolean result = ShopItemServicePostItemTest.instance.postItem(this.name, this.price, this.amount);
        assertEquals(expResult, result);
    }
}
