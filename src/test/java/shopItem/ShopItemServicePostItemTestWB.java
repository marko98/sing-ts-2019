/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import service.ShopItemService;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class ShopItemServicePostItemTestWB {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    
    private String name, message;
    private boolean expectedResult;
    private float price;
    private int amount;
    
    public ShopItemServicePostItemTestWB(String name, float price, int amount, boolean expectedResult, String message) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServicePostItemTestWB.shopItemDAO = new ShopItemDAO();
        ShopItemServicePostItemTestWB.instance = new ShopItemService();
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<ShopItem> shopItems = ShopItemServicePostItemTestWB.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServicePostItemTestWB.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ShopItemServicePostItemTestWB.shopItemDAO = null;
        ShopItemServicePostItemTestWB.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            String name, float price, int amount, boolean expectedResult, String message
            {"olovka", -50, -100, false, "D name: 0, U: 1, ime proizvoda nije prazno, cena < 0, kolicina < 0, ocekujemo false"},
            {"olovka", 0, 0, true, "D name: 0, U: 3, ime proizvoda nije prazno, cena = 0, kolicina = 0, ocekujemo true"},
//            sa ova dva testa su pokriveni i slucajevi
            /**
             * D price: 0, U: 1
             * D price: 0, U: 3
             * 
             * D amount: 0, U: 1
             * D amount: 0, U: 3
             */
        });
    }

    /**
     * Test of postItem method, of class ShopItemService.
     */
    /**
     * Testiramo postItem metodu ShopItemService-a (pokrivanje DU putanja)
     * 
     * metoda prima:
     * 
     * - prima ime, cenu i kolicinu
     * 
     * metoda vraca:
     * 
     * - ukoliko ni jedno polje nije prazno i cena, niti kolicina nisu negativne, 
     * vraca se true, u suprotnom false
     */
    @Test
    public void testPostItemSISDUPutanje() {
        System.out.println("testPostItemSISDUPutanje " + this.message);
        
        boolean expResult = this.expectedResult;
        boolean result = ShopItemServicePostItemTestWB.instance.postItem(this.name, this.price, this.amount);
        assertEquals(expResult, result);
    }
    
}
