/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemServiceStockUpTest {
    private static ShopItemService instance;
    private static ShopItemDAO shopItemDAO;
    
    public ShopItemServiceStockUpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceStockUpTest.instance = new ShopItemService();
        ShopItemServiceStockUpTest.shopItemDAO = new ShopItemDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        ShopItemServiceStockUpTest.instance = null;
        ShopItemServiceStockUpTest.shopItemDAO = null;
    }
    
    @Before
    public void setUp() {
        ShopItem object = new ShopItem();
        object.setName("Muski sampon za kosu");
        object.setAmount(200);
        object.setPrice(100);
        
        boolean expResult = true;
        boolean result = ShopItemServiceStockUpTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<ShopItem> shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceStockUpTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
    }

    /**
     * Test of stockUp method, of class ShopItemService.
     */
    /**
     * Testiramo stockUp metodu ShopItemService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima shopItem i kolicinu za uvecanje
     * 
     * metoda ne vraca nista vec:
     * 
     * - ukoliko količina nije negativna, na postojeću količinu proizvoda se 
     * doda nova, i sačuva se u bazi
     * 
     * 
     * Granicne vrednosti i klase ekvivalencije
     * 
     * legalne:
     *  kolicina >= 0
     * ilegalne:
     *  kolicina < kolicina zeljenog priozivoda na lageru
     */
    @Test
    public void testSISStockUpTOUPGGVKEC0C2P1() {
        System.out.println("testSISStockUpTOUPGGVKE C0C2P1 - prosledjen ShopItem"
                + "i kolicina koja je negativna, ne ocekujemo izmene nad proizvodom niti u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount();
        
        ShopItemServiceStockUpTest.instance.stockUp(onlyOne, -500);
        
//        ne ocekujemo izmene nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISStockUpTOUPGGVKEC0C3P1() {
        System.out.println("testSISStockUpTOUPGGVKE C0C3P1 - prosledjen ShopItem"
                + "i kolicina koja je 0, ne ocekujemo izmene nad proizvodom niti u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount();
        
        ShopItemServiceStockUpTest.instance.stockUp(onlyOne, 0);
        
//        ne ocekujemo izmene nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISStockUpTOUPGGVKEC0C4P0() {
        System.out.println("testSISStockUpTOUPGGVKE C0C4P0 - prosledjen ShopItem"
                + "i kolicina koja je pozitivna, ocekujemo izmene nad proizvodom niti u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount() + 1000000000;
        
        ShopItemServiceStockUpTest.instance.stockUp(onlyOne, 1000000000);
        
//        ocekujemo izmene nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ocekujemo izmene u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISStockUpTOUPGGVKEC1C4P1() {
        System.out.println("testSISStockUpTOUPGGVKE C1C4P1 - prosledjen ShopItem koji ne postoji"
                + "i kolicina koja je pozitivna, ne ocekujemo izmene nad proizvodom niti u bazi");
        
        ShopItem object = new ShopItem();
        object.setName("Muski sampon za kosu");
        object.setAmount(200);
        object.setPrice(100);
        
        int expectedAmount = object.getAmount();
        
        ShopItemServiceStockUpTest.instance.stockUp(object, 1000000000);
        
//        ocekujemo izmene nad proizvodom
        assertEquals(expectedAmount, object.getAmount());
        
        ArrayList<ShopItem> shopItems = ShopItemServiceStockUpTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ocekujemo izmene u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
}
