/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemServiceRemoveItemTest {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    
    public ShopItemServiceRemoveItemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceRemoveItemTest.shopItemDAO = new ShopItemDAO();
        ShopItemServiceRemoveItemTest.instance = new ShopItemService();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        ShopItemServiceRemoveItemTest.shopItemDAO = null;
        ShopItemServiceRemoveItemTest.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of removeItem method, of class ShopItemService.
     */
    /**
     * Testiramo removeItem metodu ShopItemService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima shopItem
     * 
     * metoda vraca:
     * 
     * - ukoliko prosledjeni shopItem ima id, 
     * vraca se true, u suprotnom false
     */
    @Test
    public void testSISRemoveItemC0P0() {
        System.out.println("testSISRemoveItem C0P0 - ima id, ocekujemo true");
        ShopItem s = new ShopItem();
        s.setId(1);
        
        boolean expResult = true;
        boolean result = ShopItemServiceRemoveItemTest.instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSISRemoveItemC1P1() {
        System.out.println("testSISRemoveItem C1P1 - nema id, ocekujemo false");
        ShopItem s = new ShopItem();
        
        boolean expResult = false;
        boolean result = ShopItemServiceRemoveItemTest.instance.removeItem(s);
        assertEquals(expResult, result);
    }
   
}
