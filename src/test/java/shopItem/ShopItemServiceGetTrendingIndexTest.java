/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemServiceGetTrendingIndexTest {
    private static ShopItemService instance;
    private static ShopItemDAO shopItemDAO;
    private static TransactionDAO transactionDAO;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ClientDAO clientDAO;
    
    public ShopItemServiceGetTrendingIndexTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceGetTrendingIndexTest.instance = new ShopItemService();
        ShopItemServiceGetTrendingIndexTest.shopItemDAO = new ShopItemDAO();
        ShopItemServiceGetTrendingIndexTest.transactionDAO = new TransactionDAO();
        ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO = new DeliveryServiceDAO();
        ShopItemServiceGetTrendingIndexTest.clientDAO = new ClientDAO();
        
//        napravimo jednog klijenta
        Client client = new Client();
        client.setName("Marko");
        client.setUsername("marko");
        client.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ShopItemServiceGetTrendingIndexTest.clientDAO.insertOne(client);
        assertEquals(expResult, result);
        
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(100);
        
        expResult = true;
        result = ShopItemServiceGetTrendingIndexTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
        
//      transakcije pravimo kasnije
       
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {        
        ArrayList<DeliveryService> deliveryServices = ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<ShopItem> shopItems = ShopItemServiceGetTrendingIndexTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceGetTrendingIndexTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<Client> clients = ShopItemServiceGetTrendingIndexTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceGetTrendingIndexTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ShopItemServiceGetTrendingIndexTest.instance = null;
        ShopItemServiceGetTrendingIndexTest.shopItemDAO = null;
        ShopItemServiceGetTrendingIndexTest.transactionDAO = null;
        ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO = null;
        ShopItemServiceGetTrendingIndexTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        ArrayList<Transaction> transactions = ShopItemServiceGetTrendingIndexTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceGetTrendingIndexTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
    }
    
    /**
     * Test of getTrendingIndex method, of class ShopItemService.
     */
    /**
     * Testiramo getTrendingIndex metodu ShopItemService-a (Granicne vrednosti i klase ekvivalencije)
     * 
     * metoda prima:
     * 
     * - prima shopItem
     * 
     * ukoliko proizvod nema id, odnosno nije validan proizvoda, vraća IllegalArgumentException
     * 
     * metoda vraca:
     * 
     * - trending index (float)
     * index se racuna po formuli:
     * 
     * indeksTrazenosti = ukupanProfit/brojDanaOdPoslednjeKupovine
     * 
     * legalne:
     *  id shopItem-a postoji
     *  ukupanProfit >= 0
     *  brojDanaOdPoslednjeKupovine >= 0 (obratiti paznju na deljenje sa 0)
     * ilegalne:
     *  id shopItem-a ne postoji
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSISGetTrendingIndexGVIdNePostoji() {
        System.out.println("testSISGetTrendingIndexGVIdNePostoji - id shopItem-a ne postoji, ocekujemo IllegalArgumentException");
        
        ShopItem shopItem = new ShopItem();
                
        float result = ShopItemServiceGetTrendingIndexTest.instance.getTrendingIndex(shopItem);
    }
    
    @Test
    public void testSISGetTrendingIndexGVIdPostojiUkupanProfit0() {
        System.out.println("testSISGetTrendingIndexGVIdPostojiUkupanProfit0 - id shopItem-a postoji, ukupanProfit je 0, ocekujemo rezultat 0");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceGetTrendingIndexTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        float expResult = 0.0F;
        float result = ShopItemServiceGetTrendingIndexTest.instance.getTrendingIndex(onlyOne);
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testSISGetTrendingIndexGVIdPostojiUkupanProfitVeciOd0BrojDanaOdPoslednjeKupovine10() {
        System.out.println("testSISGetTrendingIndexGVIdPostojiUkupanProfitVeciOd0BrojDanaOdPoslednjeKupovine10 - id shopItem-a postoji, "
                + "ukupanProfit je veci od 0, brojDanaOdPoslednjeKupovine je 10, ne ocekujemo rezultat 0");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceGetTrendingIndexTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
//        na lageru ima 200
        int amountToBuy = 10;        
        float ukupanProfit = amountToBuy * onlyOne.getPrice();
        int brojDanaOdPoslednjeKupovine = 10;
        
        LocalDate localDate = LocalDate.now().minusDays(brojDanaOdPoslednjeKupovine);
        Date date = Date.valueOf(localDate);
        createTransaction(amountToBuy, date);
        
        float expResult = ukupanProfit/brojDanaOdPoslednjeKupovine;
        float result = ShopItemServiceGetTrendingIndexTest.instance.getTrendingIndex(onlyOne);
        assertEquals(expResult, result, 0.0);
    }
    
    @Test
    public void testSISGetTrendingIndexGVIdPostojiUkupanProfitVeciOd0BrojDanaOdPoslednjeKupovine0() {
        System.out.println("testSISGetTrendingIndexGVIdPostojiUkupanProfitVeciOd0BrojDanaOdPoslednjeKupovine0 - id shopItem-a postoji, "
                + "ukupanProfit je veci od 0, brojDanaOdPoslednjeKupovine je 0, ocekujemo rezultat 0");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceGetTrendingIndexTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
//        na lageru ima 200
        int amountToBuy = 10;        
        float ukupanProfit = amountToBuy * onlyOne.getPrice();
        int brojDanaOdPoslednjeKupovine = 0;
        
        LocalDate localDate = LocalDate.now().minusDays(brojDanaOdPoslednjeKupovine);
        Date date = Date.valueOf(localDate);
        createTransaction(amountToBuy, date);
        
        float result = ShopItemServiceGetTrendingIndexTest.instance.getTrendingIndex(onlyOne);
        float expResult = ukupanProfit/brojDanaOdPoslednjeKupovine;
        assertEquals(expResult, result, 0.0);
    }
    
    public void createTransaction(int amountToBuy, Date date){
        ArrayList<Client> clients = ShopItemServiceGetTrendingIndexTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = ShopItemServiceGetTrendingIndexTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = ShopItemServiceGetTrendingIndexTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
//        napravimo jednu transakcije
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(amountToBuy); // ukupno ima 200 komada
        transaction.setDate(date);
//        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = ShopItemServiceGetTrendingIndexTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
}
