/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.ClientDAO;
import db.DatabaseConnection;
import db.DeliveryServiceDAO;
import db.ShopItemDAO;
import db.TransactionDAO;
import db.TransactionDAOTest;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author marko
 */
@RunWith(Parameterized.class)
public class ShopItemServiceCheckIfPopularTest {
    private static ShopItemService instance;
    private static ShopItemDAO shopItemDAO;
    private static TransactionDAO transactionDAO;
    private static DeliveryServiceDAO deliveryServiceDAO;
    private static ClientDAO clientDAO;
    
    private float price;
    private int days, amountToBuy;
    private String message;
    private boolean expectedResult;
    
    public ShopItemServiceCheckIfPopularTest(float price, int amountToBuy, int days, boolean expectedResult, String message) {
        this.price = price;
        this.amountToBuy = amountToBuy;
        this.days = days;
        this.expectedResult = expectedResult;
        this.message = message;
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceCheckIfPopularTest.instance = new ShopItemService();
        ShopItemServiceCheckIfPopularTest.shopItemDAO = new ShopItemDAO();
        ShopItemServiceCheckIfPopularTest.transactionDAO = new TransactionDAO();
        ShopItemServiceCheckIfPopularTest.deliveryServiceDAO = new DeliveryServiceDAO();
        ShopItemServiceCheckIfPopularTest.clientDAO = new ClientDAO();
        
//        napravimo jednog klijenta
        Client client = new Client();
        client.setName("Marko");
        client.setUsername("marko");
        client.setPassword("marko");
        
        boolean expResult = true;
        boolean result = ShopItemServiceCheckIfPopularTest.clientDAO.insertOne(client);
        assertEquals(expResult, result);
        
//      prozivode i transakcije pravimo kasnije
       
//        napravimo jednu dostavu
        DeliveryService deliveryService = new DeliveryService();
        deliveryService.setName("Amazaon"); // UPS
        deliveryService.setStartingPrice(1000);
        deliveryService.setPricePerKilometer(200);
        
        expResult = true;
        result = ShopItemServiceCheckIfPopularTest.deliveryServiceDAO.insertOne(deliveryService);
        assertEquals(expResult, result);
    }
    
    @AfterClass
    public static void tearDownClass() {
        ArrayList<DeliveryService> deliveryServices = ShopItemServiceCheckIfPopularTest.deliveryServiceDAO.getAll();
//        brisemo dostavljace dodate u bazi
        deliveryServices.stream().forEach((deliveryService) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceCheckIfPopularTest.deliveryServiceDAO.deleteOne(deliveryService.getId());
            assertEquals(expResult, result);
        });
        
        ArrayList<Client> clients = ShopItemServiceCheckIfPopularTest.clientDAO.getAll();
//        brisemo korisnike dodate u bazi
        clients.stream().forEach((client) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceCheckIfPopularTest.clientDAO.deleteOne(client.getId());
            assertEquals(expResult, result);
        }); 
        
        DatabaseConnection.close();
        ShopItemServiceCheckIfPopularTest.instance = null;
        ShopItemServiceCheckIfPopularTest.shopItemDAO = null;
        ShopItemServiceCheckIfPopularTest.transactionDAO = null;
        ShopItemServiceCheckIfPopularTest.deliveryServiceDAO = null;
        ShopItemServiceCheckIfPopularTest.clientDAO = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        ArrayList<Transaction> transactions = ShopItemServiceCheckIfPopularTest.transactionDAO.getAll();
//        brisemo transakcije dodate u bazi
        transactions.stream().forEach((transaction) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceCheckIfPopularTest.transactionDAO.deleteOne(transaction.getId());
            assertEquals(expResult, result);
        }); 
        
        ArrayList<ShopItem> shopItems = ShopItemServiceCheckIfPopularTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceCheckIfPopularTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
    }
    
    @Parameterized.Parameters
    public static Collection paramters(){
        return Arrays.asList(new Object[][]{
//            float price, int amountToBuy, int days, boolean expectedResult, String message
            
//            transakcija se desila pre tacno 30 dana
            {400, 120, 30, false, "C0C4C9P1 - cena proizvoda > 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.6 -> 120
            {400, 140, 30, true, "C0C3C9P0 - cena proizvoda > 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo true"},// 200*0.7 -> 140
            {400, 100, 30, false, "C0C5C9P1 - cena proizvoda > 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.5 -> 100
            
            {200, 160, 30, false, "C2C7C9P1 - cena proizvoda < 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.8 -> 160
            {200, 180, 30, true, "C2C6C9P0 - cena proizvoda < 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo true"},// 200*0.9 -> 180
            {200, 140, 30, false, "C2C8C9P1 - cena proizvoda < 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.7 -> 140
            
            {300, 120, 30, false, "C1C4C9P1 - cena proizvoda je 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.6 -> 120
            {300, 140, 30, false, "C1C3C9P1 - cena proizvoda je 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.7 -> 140
            {300, 100, 30, false, "C1C5C9P1 - cena proizvoda je 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.5 -> 100
        
            {300, 160, 30, false, "C1C7C9P1 - cena proizvoda je 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.8 -> 160
            {300, 180, 30, false, "C1C6C9P1 - cena proizvoda je 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.9 -> 180
            {300, 140, 30, false, "C1C8C9P1 - cena proizvoda je 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 30 dana, ocekujemo false"},// 200*0.7 -> 140
            
//            transakcija se desila pre tacno 40 dana
            {400, 120, 40, false, "C0C4C10P1 - cena proizvoda > 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.6 -> 120
            {400, 140, 40, false, "C0C3C10P1 - cena proizvoda > 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.7 -> 140
            {400, 100, 40, false, "C0C5C10P1 - cena proizvoda > 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.5 -> 100
            
            {200, 160, 40, false, "C2C7C10P1 - cena proizvoda < 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.8 -> 160
            {200, 180, 40, false, "C2C6C10P1 - cena proizvoda < 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.9 -> 180
            {200, 140, 40, false, "C2C8C10P1 - cena proizvoda < 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.7 -> 140
            
            {300, 120, 40, false, "C1C4C10P1 - cena proizvoda je 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.6 -> 120
            {300, 140, 40, false, "C1C3C10P1 - cena proizvoda je 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.7 -> 140
            {300, 100, 40, false, "C1C5C10P1 - cena proizvoda je 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.5 -> 100
        
            {300, 160, 40, false, "C1C7C10P1 - cena proizvoda je 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.8 -> 160
            {300, 180, 40, false, "C1C6C10P1 - cena proizvoda je 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.9 -> 180
            {300, 140, 40, false, "C1C8C10P1 - cena proizvoda je 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 40 dana, ocekujemo false"},// 200*0.7 -> 140
            
//            transakcija se desila pre tacno 20 dana
            {400, 120, 20, false, "C0C4C11P1 - cena proizvoda > 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.6 -> 120
            {400, 140, 20, true, "C0C3C11P0 - cena proizvoda > 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo true"},// 200*0.7 -> 140
            {400, 100, 20, false, "C0C5C11P1 - cena proizvoda > 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.5 -> 100
            
            {200, 160, 20, false, "C2C7C11P1 - cena proizvoda < 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.8 -> 160
            {200, 180, 20, true, "C2C6C11P0 - cena proizvoda < 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo true"},// 200*0.9 -> 180
            {200, 140, 20, false, "C2C8C11P1 - cena proizvoda < 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.7 -> 140
            
            {300, 120, 20, false, "C1C4C11P1 - cena proizvoda je 300, kupljeno 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.6 -> 120
            {300, 140, 20, false, "C1C3C11P1 - cena proizvoda je 300, kupljeno > 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.7 -> 140
            {300, 100, 20, false, "C1C5C11P1 - cena proizvoda je 300, kupljeno < 60% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.5 -> 100
        
            {300, 160, 20, false, "C1C7C11P1 - cena proizvoda je 300, kupljeno 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.8 -> 160
            {300, 180, 20, false, "C1C6C11P1 - cena proizvoda je 300, kupljeno > 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.9 -> 180
            {300, 140, 20, false, "C1C8C11P1 - cena proizvoda je 300, kupljeno < 80% trenutnih zaliha, transakcija se desila pre tacno 20 dana, ocekujemo false"},// 200*0.7 -> 140
        });
    }

    /**
     * Test of checkIfPopular method, of class ShopItemService.
     */
    /**
     * Testiramo checkIfPopular metodu ShopItemService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima shopItem
     * 
     * metoda vraca:
     * 
     * - ukoliko je proizvod popularan vraca se true, u suprotnom false
     * 
     * Proizvod je popularan ako:
     * 
     * – je njegova cena veća od 300 i količina kupljena u prethodnih 30 dana je veća od 60% trenutnih zaliha proizvoda, ili
     * 
     * – njegova cena je manja od 300, i količina kupljena u prethodnih 30 dana je veća od 80% trenutnih zaliha proizvoda
     */
    @Test
    public void testSISCheckIfPopularTOUPGGV() {
        System.out.println("testSISCheckIfPopularTOUPGGV " + this.message);
        
//        kreiramo proizvod na osnovu cene
        createShopItem(this.price);
        ArrayList<ShopItem> shopItems = ShopItemServiceCheckIfPopularTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        LocalDate localDate = LocalDate.now().minusDays(this.days);
        Date date = Date.valueOf(localDate);
//        kreiramo transkaciju
        createTransaction(this.amountToBuy, date);
        
        boolean result = ShopItemServiceCheckIfPopularTest.instance.checkIfPopular(onlyOne);
        assertEquals(this.expectedResult, result);
    }
    
    public void createShopItem(float price){
//        napravimo jedan proizvod
        ShopItem shopItem = new ShopItem();
        shopItem.setName("Muski sampon za kosu");
        shopItem.setAmount(200);
        shopItem.setPrice(price);
        
        boolean expResult = true;
        boolean result = ShopItemServiceCheckIfPopularTest.shopItemDAO.insertOne(shopItem);
        assertEquals(expResult, result);
    }
    
    public void createTransaction(int amountToBuy, Date date){
        ArrayList<Client> clients = ShopItemServiceCheckIfPopularTest.clientDAO.getAll();
        ArrayList<ShopItem> shopItems = ShopItemServiceCheckIfPopularTest.shopItemDAO.getAll();
        ArrayList<DeliveryService> deliveryServices = ShopItemServiceCheckIfPopularTest.deliveryServiceDAO.getAll();
        
        Client client = clients.get(0);
        ShopItem shopItem = shopItems.get(0);
        DeliveryService deliveryService = deliveryServices.get(0);
        
//        napravimo jednu transakcije
        Transaction transaction = new Transaction();
        transaction.setShopItemId(shopItem.getId());
        transaction.setClientId(client.getId());
        transaction.setDeliveryServiceId(deliveryService.getId());
        transaction.setAmount(amountToBuy); // ukupno ima 200 komada
        transaction.setDate(date);
//        transaction.setDate(new Date(System.currentTimeMillis()));
        transaction.setDistance(1000);
        transaction.setTotalPrice(1100);
        
        boolean expResult = true;
        boolean result = ShopItemServiceCheckIfPopularTest.transactionDAO.insertOne(transaction);
        assertEquals(expResult, result);
    }
    
}
