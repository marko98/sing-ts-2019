/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemServiceRemoveItemTestWB {
    private static ShopItemDAO shopItemDAO;
    private static ShopItemService instance;
    
    public ShopItemServiceRemoveItemTestWB() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceRemoveItemTestWB.shopItemDAO = new ShopItemDAO();
        ShopItemServiceRemoveItemTestWB.instance = new ShopItemService();
    }
    
    @AfterClass
    public static void tearDownClass() {
        DatabaseConnection.close();
        ShopItemServiceRemoveItemTestWB.shopItemDAO = null;
        ShopItemServiceRemoveItemTestWB.instance = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of removeItem method, of class ShopItemService.
     */
    /**
     * Testiramo removeItem metodu ShopItemService-a (pokrivanje DU putanja)
     * 
     * metoda prima:
     * 
     * - prima shopItem
     * 
     * metoda vraca:
     * 
     * - ukoliko prosledjeni shopItem ima id, 
     * vraca se true, u suprotnom false
     */    
    @Test
    public void testSISRemoveItemDUPutanjeDS0U1() {
        System.out.println("testSISRemoveItemDUPutanje DS0U1 - ima id, ocekujemo true");
        ShopItem s = new ShopItem();
        s.setId(1);
        
        boolean expResult = true;
        boolean result = ShopItemServiceRemoveItemTestWB.instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSISRemoveItemDUPutanjeDS0U3() {
        System.out.println("testSISRemoveItemDUPutanje DS0U3 - nema id, ocekujemo false");
        ShopItem s = new ShopItem();
        
        boolean expResult = false;
        boolean result = ShopItemServiceRemoveItemTestWB.instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
}
