/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopItem;

import db.DatabaseConnection;
import db.ShopItemDAO;
import db.ShopItemDAOTest;
import java.util.ArrayList;
import model.ShopItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import service.ShopItemService;
import static org.junit.Assert.*;

/**
 *
 * @author marko
 */
public class ShopItemServiceBuyTest {
    private static ShopItemService instance;
    private static ShopItemDAO shopItemDAO;
    
    public ShopItemServiceBuyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        DatabaseConnection.getConnection();
        ShopItemServiceBuyTest.instance = new ShopItemService();
        ShopItemServiceBuyTest.shopItemDAO = new ShopItemDAO();
    }
    
    @AfterClass
    public static void tearDownClass() {        
        DatabaseConnection.close();
        ShopItemServiceBuyTest.instance = null;
        ShopItemServiceBuyTest.shopItemDAO = null;
    }
    
    @Before
    public void setUp() {
        ShopItem object = new ShopItem();
        object.setName("Muski sampon za kosu");
        object.setAmount(200);
        object.setPrice(100);
        
        boolean expResult = true;
        boolean result = ShopItemServiceBuyTest.shopItemDAO.insertOne(object);
        assertEquals(expResult, result);
    }
    
    @After
    public void tearDown() {
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
//        brisemo proizvode dodate u bazi
        shopItems.stream().forEach((shopItem) -> {
            boolean expResult = true;
            boolean result = ShopItemServiceBuyTest.shopItemDAO.deleteOne(shopItem.getId());
            assertEquals(expResult, result);
        });
    }

    /**
     * Test of buy method, of class ShopItemService.
     */
    /**
     * Testiramo buy metodu ShopItemService-a (Tabla odlucivanja i uzrocno-posledicni graf)
     * 
     * metoda prima:
     * 
     * - prima shopItem i zeljenu kolicinu
     * 
     * metoda ne vraca nista vec:
     * 
     * - proverava se da li je prosleđen negativan broj za količinu, 
     * pa se čuva u bazi ukoliko nije
     * 
     * 
     * Granicne vrednosti i klase ekvivalencije
     * 
     * legalne:
     *  kolicina >= kolicina zeljenog priozivoda na lageru
     * ilegalne:
     *  kolicina < kolicina zeljenog priozivoda na lageru
     */
    @Test
    public void testSISBuyTOUPGGVKEC0C2P1() {
        System.out.println("testSISBuyTOUPGGVKE C0C2P1 - prosledjen "
                + "ShopItem i kolicina koja je negativna, "
                + "ne ocekujemo izmene(amount) nad proizvodom, niti u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount();
        ShopItemServiceBuyTest.instance.buy(onlyOne, -100);
        
//        ne ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISBuyTOUPGGVKEC0C3P1() {
        System.out.println("testSISBuyTOUPGGVKE C0C3P1 - "
                + "prosledjen ShopItem i kolicina koja je pozitivna i veca od kolicine na lageru, "
                + "ne ocekujemo izmene(amount) nad proizvodom, niti u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount();
        ShopItemServiceBuyTest.instance.buy(onlyOne, onlyOne.getAmount() + 100);
        
//        ne ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISBuyTOUPGGVKEC0C4P0() {
        System.out.println("testSISBuyTOUPGGVKE C0C4P0 - "
                + "prosledjen ShopItem i kolicina koja je pozitivna i jednaka kolicini na lageru, "
                + "ocekujemo izmene(amount) nad proizvodom i u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = 0;
        ShopItemServiceBuyTest.instance.buy(onlyOne, onlyOne.getAmount());
        
//        ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISBuyTOUPGGVKEC0C5P0() {
        System.out.println("testSISBuyTOUPGGVKE C0C5P0 - "
                + "prosledjen ShopItem i kolicina koja je pozitivna i manja od kolicine na lageru, "
                + "ocekujemo izmene(amount) nad proizvodom, i u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
//        znamo da je dostupno 200 komada
        int expectedAmount = onlyOne.getAmount() - (onlyOne.getAmount() - 50);
        
        ShopItemServiceBuyTest.instance.buy(onlyOne, onlyOne.getAmount() - 50);
        
//        ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISBuyTOUPGGVKEC0C6P1() {
        System.out.println("testSISBuyTOUPGGVKE C0C6P1 - "
                + "prosledjen ShopItem i kolicina koja je pozitivna, 0, "
                + "ne ocekujemo izmene(amount) nad proizvodom u bazi");
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem onlyOne = shopItems.get(0);
        
        int expectedAmount = onlyOne.getAmount();
        ShopItemServiceBuyTest.instance.buy(onlyOne, 0);
        
//        ne ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, onlyOne.getAmount());
        
        shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
    
    @Test
    public void testSISBuyTOUPGGVKEC1C5P1() {
        System.out.println("testSISBuyTOUPGGVKE C1C5P1 - "
                + "prosledjen ShopItem koji ne postoji i kolicina koja je pozitivna i manja od kolicine na lageru, "
                + "ne ocekujemo izmene(amount) nad proizvodom, niti u bazi");
        
        ShopItem object = new ShopItem();
        object.setName("Muski sampon za kosu");
        object.setAmount(200);
        object.setPrice(100);
        
//        znamo da je dostupno 200 komada
        int expectedAmount = object.getAmount();
        
        ShopItemServiceBuyTest.instance.buy(object, object.getAmount() - 50);
        
//        ne ocekujemo izmene(amount) nad proizvodom
        assertEquals(expectedAmount, object.getAmount());
        
        ArrayList<ShopItem> shopItems = ShopItemServiceBuyTest.shopItemDAO.getAll();
        ShopItem updatedOne = shopItems.get(0);
        
//        ne ocekujemo izmene(amount) u bazi
        assertEquals(expectedAmount, updatedOne.getAmount());
    }
}
