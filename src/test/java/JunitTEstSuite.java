


import client.ClientServiceDeleteUserTest;
import client.ClientServiceLoginTest;
import client.ClientServiceRegisterTest;
import client.ClientServiceUpdateInfoTest;
import deliveryService.DeliveryServiceServiceDeleteDeliveryServiceTest;
import deliveryService.DeliveryServiceServiceRegisterTest;
import deliveryService.DeliveryServiceServiceUpdateInfoTest;
import shopItem.ShopItemServiceBuyTest;
import shopItem.ShopItemServiceCheckIfPopularTest;
import shopItem.ShopItemServiceGetTrendingIndexTest;
import shopItem.ShopItemServicePostItemTest;
import shopItem.ShopItemServiceRemoveItemTest;
import shopItem.ShopItemServiceStockUpTest;
import transactionService.TransactionServiceCompleteTransactionTest;
import transactionService.TransactionServiceGetRecentTransactionsTest;
import db.DatabaseConnectionTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import client.ClientServiceDeleteUserTestWB;
import deliveryService.DeliveryServiceServiceRegisterTestWB;
import shopItem.ShopItemServicePostItemTestWB;
import shopItem.ShopItemServiceRemoveItemTestWB;
import transactionService.TransactionServiceCalculatePriceTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marko
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
//    provera konekcije
    DatabaseConnectionTest.class,
//    client tests
    ClientServiceDeleteUserTest.class,
    ClientServiceDeleteUserTestWB.class,
    ClientServiceLoginTest.class,
    ClientServiceRegisterTest.class,
    ClientServiceUpdateInfoTest.class,
//    delivery service tests
    DeliveryServiceServiceDeleteDeliveryServiceTest.class,
    DeliveryServiceServiceRegisterTest.class,
    DeliveryServiceServiceRegisterTestWB.class,
    DeliveryServiceServiceUpdateInfoTest.class,
//    shop item tests
    ShopItemServiceBuyTest.class,
    ShopItemServiceCheckIfPopularTest.class,
    ShopItemServiceGetTrendingIndexTest.class,
    ShopItemServicePostItemTest.class,
    ShopItemServicePostItemTestWB.class,
    ShopItemServiceRemoveItemTest.class,
    ShopItemServiceRemoveItemTestWB.class,
    ShopItemServiceStockUpTest.class,
//    transaction tests
    TransactionServiceCompleteTransactionTest.class,
    TransactionServiceGetRecentTransactionsTest.class,
    TransactionServiceCalculatePriceTest.class
})
public class JunitTEstSuite {
    
}
